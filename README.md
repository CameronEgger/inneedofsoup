Presentation link (pdf copy in in repo): https://docs.google.com/presentation/d/1rS1weaidgG6Zk8Qqiq5k-IkHxWTdZYsrT3dx5fVj5ws/edit?usp=sharing  

   
Phase 4:  
**Estimated time of completion: 20 hours per member**  
**Acutal time of completion: 12 hours per member**    
  
Visualizations:
The D3 visualizations are presented in the D3 tab.
Phase 3:  
  
**Estimated time of completion: 25 hours per member**  
**Actual time of completion: 30 hours per member**  


**Member Info**


Name: Tongrui Li

EID: TL25693

GitlabID: tongrui1998


Name: Ethan Arnold

EID: EA24869

GitlabID: masonsbro456


Name: Henry Zhang

EID: HZ3953

GitlabID: henryzhang99


Name: David Lambert

EID: DEL892

GitlabID: dlambert124


Name: Cameron Egger

EID: CME977

GitlabID: CameronEgger


**Git SHA (Phase4):** 6f78af26  

**link to pipeline:** https://gitlab.com/CameronEgger/inneedofsoup/pipelines

**link to website:** www.inneedofsoup.xyz   

**Comments:** 
backend/tests.py has some tests that were heavily modeled off of the tests example given on Piazza by the TA's.

**Postman**
https://documenter.getpostman.com/view/6781278/S11LsdCb#intro  

