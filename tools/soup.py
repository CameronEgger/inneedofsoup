import requests

base_url = "http://localhost:8000/api"

INF = 500

with open("../keys/soup") as f:
    auth_key = f.read().strip()


def get_soup_kitchens():
    res = requests.get(base_url + "/soup_kitchen", params={"results_per_page": INF})
    assert res.status_code == 200
    return res.json()["objects"]


def get_foods():
    res = requests.get(base_url + "/food", params={"results_per_page": INF})
    assert res.status_code == 200
    return res.json()["objects"]


def get_donation_opportunities():
    res = requests.get(
        base_url + "/donation_opportunity", params={"results_per_page": INF}
    )
    assert res.status_code == 200
    return res.json()["objects"]


def add_soup_kitchen(data):
    res = requests.post(base_url + "/soup_kitchen", json={**data, "auth_key": auth_key})
    assert res.status_code == 201


def add_food(data):
    res = requests.post(base_url + "/food", json={**data, "auth_key": auth_key})
    assert res.status_code == 201


def add_donation_opportunity(data):
    res = requests.post(
        base_url + "/donation_opportunity", json={**data, "auth_key": auth_key}
    )
    assert res.status_code == 201


def delete_all_soup_kitchens():
    res = requests.delete(base_url + "/soup_kitchen", json={"auth_key": auth_key})
    assert res.status_code == 200


def delete_all_foods():
    res = requests.delete(base_url + "/food", json={"auth_key": auth_key})
    assert res.status_code == 200


def delete_all_donation_opportunities():
    res = requests.delete(
        base_url + "/donation_opportunity", json={"auth_key": auth_key}
    )
    assert res.status_code == 200


def delete_all_soup_kitchen_food_relationships():
    res = requests.delete(
        base_url + "/kitchen_food_association", json={"auth_key": auth_key}
    )
    assert res.status_code == 200


def add_soup_kitchen_food_relationship(soup_kitchen_ein, food_id):
    res = requests.post(
        base_url + "/kitchen_food_association",
        json={
            "soup_kitchen_ein": soup_kitchen_ein,
            "food_id": food_id,
            "auth_key": auth_key,
        },
    )
    assert res.status_code == 201


def delete_all_soup_kitchen_volunteer_relationships():
    res = requests.delete(
        base_url + "/kitchen_volunteer_association", json={"auth_key": auth_key}
    )
    assert res.status_code == 200


def add_soup_kitchen_volunteer_relationship(soup_kitchen_ein, volunteer_ein):
    res = requests.post(
        base_url + "/kitchen_volunteer_association",
        json={
            "soup_kitchen_ein": soup_kitchen_ein,
            "volunteer_ein": volunteer_ein,
            "auth_key": auth_key,
        },
    )
    assert res.status_code == 201


def delete_all_food_volunteer_relationships():
    res = requests.delete(
        base_url + "/volunteer_food_association", json={"auth_key": auth_key}
    )
    assert res.status_code == 200


def add_food_volunteer_relationship(food_id, volunteer_ein):
    res = requests.post(
        base_url + "/food_volunteer_association",
        json={"food_id": food_id, "volunteer_ein": volunteer_ein, "auth_key": auth_key},
    )
    assert res.status_code == 201


def reg_caps(s):
    return " ".join(map(lambda t: t.capitalize(), s.split()))
