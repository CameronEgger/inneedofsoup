import requests, time

with open("../keys/orghunter") as f:
    user_key = f.read().strip()
base_url = "http://data.orghunter.com/v1"


def get_basic_info(num_rows=100, search_term=None):
    params = {"user_key": user_key, "rows": num_rows}

    if search_term:
        params["searchTerm"] = search_term

    res = requests.get(base_url + "/charitysearch", params=params)
    assert res.status_code == 200
    return res.json()["data"]


def get_financial_info(ein):
    time.sleep(0.5)
    params = {"user_key": user_key, "ein": str(ein)}
    res = requests.get(base_url + "/charityfinancial", params=params)
    assert res.status_code == 200
    return res.json()["data"]
