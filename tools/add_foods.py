import usda, img, soup

CALORIES = 208
CARBS = 205
FAT = 204
PROTEIN = 203
FIBER = 291

NUTRIENT_NUMS = [CALORIES, CARBS, FAT, PROTEIN, FIBER]
NUTRIENTS = list(map(str, NUTRIENT_NUMS))
NUTRIENT_NAMES = ["calories", "carbs", "fat", "protein", "fiber"]


def add_foods(num_rows):
    records = []
    for row in usda.get_foods(NUTRIENTS, num_rows=num_rows):
        cur = {}
        cur["food_id"] = int(row["ndbno"])
        cur["name"] = row["name"]
        cur["measure"] = row["measure"]
        cur["grams_per_measure"] = row["weight"]
        nutrients = usda.extract_nutrients(row)
        for num, name in zip(NUTRIENT_NUMS, NUTRIENT_NAMES):
            cur[name] = nutrients[num]
            cur[name + "_per_100g"] = nutrients[num] / cur["grams_per_measure"] * 100.0
        records.append(cur)

    for i in range(len(records)):
        records[i]["img"] = img.get_best_image(records[i]["name"])

    for record in records:
        soup.add_food(record)


if __name__ == "__main__":
    add_foods(num_rows=50)
