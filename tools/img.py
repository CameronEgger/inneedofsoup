import requests

with open("../keys/azure") as f:
    api_key = f.read().strip()

base_url = "https://southcentralus.api.cognitive.microsoft.com/bing/v7.0/images/search"


def get_best_image(q):
    params = {"q": q}

    headers = {"Ocp-Apim-Subscription-Key": api_key}

    res = requests.get(base_url, params=params, headers=headers)
    assert res.status_code == 200
    try:
        return res.json()["value"][0]["contentUrl"]
    except:
        return ""
