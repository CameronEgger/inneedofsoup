import requests

with open("../keys/usda") as f:
    user_key = f.read().strip()
base_url = "http://api.nal.usda.gov/ndb"


def get_foods(nutrients, num_rows=1500):
    params = {
        "api_key": user_key,
        "max": num_rows,
        "nutrients": nutrients,
        "format": "json",
        "sort": "f",
    }

    res = requests.get(base_url + "/nutrients", params=params)
    assert res.status_code == 200
    return res.json()["report"]["foods"]


def extract_nutrients(row):
    ans = {}
    for nut in row["nutrients"]:
        try:
            ans[int(nut["nutrient_id"])] = float(nut["value"])
        except:
            ans[int(nut["nutrient_id"])] = 0.0
    return ans
