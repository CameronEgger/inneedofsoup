import orghunter, img, soup


def add_donation_opportunities(num_rows):
    records = []
    for row in orghunter.get_basic_info(num_rows=num_rows):
        cur = {}
        cur["ein"] = int(row["ein"])
        cur["name"] = row["charityName"]
        cur["city"] = soup.reg_caps(row["city"])
        cur["state"] = row["state"]
        cur["zip_code"] = row["zipCode"][:5]
        cur["website"] = row["website"]
        cur["latitude"] = float(row["latitude"])
        cur["longitude"] = float(row["longitude"])
        cur["mission_statement"] = row["missionStatement"]
        records.append(cur)

    for i in range(len(records)):
        ein = records[i]["ein"]
        details = orghunter.get_financial_info(ein)
        records[i]["address"] = soup.reg_caps(details["street"])
        records[i]["deductibility"] = details["deductibility"]
        records[i]["accepting_donations"] = details["acceptingDonations"] == "1"
        records[i]["assets"] = float(details["assetAmount"])
        records[i]["income"] = float(details["incomeAmount"])
        records[i]["img"] = img.get_best_image(records[i]["name"] + " charity")

    for record in records:
        soup.add_donation_opportunity(record)


if __name__ == "__main__":
    add_donation_opportunities(num_rows=50)
