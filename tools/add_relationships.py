import random
import soup


def add_relationships(num, get_a, get_b, add_rel, a_key, b_key):
    a = get_a()
    b = get_b()
    b_did = [set() for i in range(len(b))]
    for i, x in enumerate(a):
        cur = set()
        while len(cur) < num:
            cur.add(random.randint(0, len(b) - 1))
        for j in cur:
            b_did[j].add(i)
            add_rel(x[a_key], b[j][b_key])
    for i, y in enumerate(b):
        cur = b_did[i].copy()
        while len(cur) < num:
            cur.add(random.randint(0, len(a) - 1))
        for j in cur:
            if j in b_did[i]:
                continue
            add_rel(a[j][a_key], y[b_key])


if __name__ == "__main__":
    add_relationships(
        3,
        soup.get_soup_kitchens,
        soup.get_foods,
        soup.add_soup_kitchen_food_relationship,
        "ein",
        "food_id",
    )
    add_relationships(
        3,
        soup.get_soup_kitchens,
        soup.get_donation_opportunities,
        soup.add_soup_kitchen_volunteer_relationship,
        "ein",
        "ein",
    )
    add_relationships(
        3,
        soup.get_foods,
        soup.get_donation_opportunities,
        soup.add_food_volunteer_relationship,
        "food_id",
        "ein",
    )
