import React, {Component} from 'react';
import cameron from './../static/Pictures/CameronEgger.jpg';
import Henry from './../static/Pictures/Henry.JPG';
import ethan from './../static/Pictures/ethan.jpg';
import david from './../static/Pictures/serious_photos_suck.jpeg';
import Li from './../static/Pictures/TongruiLi.jpeg';

class Contributor extends Component {
    constructor(props)  {
       super(props);
    }
    render() {
        return (
          <div class="card mb-3 bg-light shadow">
              <div class="card-body mt-4">
                  <h2 class="card-title">
                      {this.props.name} 
                  </h2>
                  <p class = "card-text">
                      {this.props.desc}
                  </p>
                  <img src="/static/Pictures/CameronEgger.jpg" alt="profile" style="width:200px;object-fit:100%">
              </div>
          </div>
        );
    }
}
const contributors = [
    {
        name: "Ethan Arnold",
        img: ethan,
        desc: "I'm a Senior and who spends too much time doing competitive programming."
    },
    {
        name: "Cameron Egger",
        img: cameron,
        desc: "I am a Junior who likes to spend time working on my Competitive Programming skills."
    },
    {
        name: "Tongrui Li",
        img: Li,
        desc: "I am a Junior who loves hardware as much as software."
    },
    {
        name: "Henry Zhang",
        img: Henry,
        desc: "I'm a Sophmore and you should ask to see a magic trick."
    },
    {
        name: "David Lambert",
        img: david,
        desc: "I am a Senior and I enjoy taking my dog everywhere I can."
    }
];
class About extends Component {
    constructor (props) {
        super(props);
        //A LOT MORE GOES HERE.
    }
    render() {
        //HIGHLY INCOMPLETE
        return(
            <body>
                <div className = "container-fluid">
                    <div className="row">
                    <div className = "col-lg-2"></div>
                    <div className = "col-lg-8 text-center">
                        <h1>About</h1>
                        <h7>The goal of this site is to provide a platform that will increase community engagement to create a better world by providing everyone with information on local soup kitchens, nutrtition, and volunteer work. The goal is for everyone to come together to make a difference, however, later it may be targeted at High Schoolers and anyone else in need of Volunteer work, as well as people in need of dietary information.</h7>
                    </div>
                    <div className = "col-lg-2"></div>
                    </div>
                    <div className ="row">
                    <div className = "col-lg-2"></div>
                    <div className = "col-lg-8 text-center">
                        <h1>Resources</h1>
                        <h7>We definitely have resources.</h7>
                    </div>
                    <div class = "col-lg-2"></div>
                    </div>
                </div>
            </body>
        );
    }
}

export default About
