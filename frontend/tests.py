import unittest
from selenium import webdriver

class tests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()

    def test_title(self):
        self.driver.get('https://inneedofsoup.xyz/')
        self.assertEqual(
            self.driver.title,
            'In Need Of Soup Bringing light to a very real problem'.)



    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()<span style="font-size: 16px;"> </span>
