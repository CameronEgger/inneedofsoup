import React, { Component } from 'react';

import Welcome from './Components/Welcome';
import Navbar from './Components/Navbar';
import Home from './Components/Home';
import Kitchens from './Components/Kitchens';
import Nutrition from './Components/Nutrition';
import Volunteer from './Components/Volunteer';
import About from './Components/About';
import KitchenSingle from './Components/KitchenSingle';
import NutritionSingle from './Components/NutritionSingle';
import VolunteerSingle from './Components/VolunteerSingle';
import GlobalSearch from './Components/GlobalSearch';
import './App.css';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Food_Scatter from './Components/ScatterD3';
import US_Heatmap from './Components/HeatmapD3';
import Income_Histogram from './Components/IncomeD3';
import DeveloperDeaths from './Components/DeveloperDeaths';
import DeveloperSanitation from './Components/DeveloperSanitation';
import DeveloperWater from './Components/DeveloperWater';

class App extends Component {
    render() {
        return (
            <Router>
                <Navbar />

                <Route path="/" exact component={Welcome} />
                <Route path="/home" exact component={Home} />
                <Route path="/kitchens" exact component={Kitchens} />
                <Route path="/nutrition" exact component={Nutrition} />
                <Route path="/volunteer" exact component={Volunteer} />
                <Route path="/about" exact component={About} />

                <Route path="/search" exact component={GlobalSearch} />


                <Route path="/kitchens/:page" exact component={Kitchens} />
                <Route path="/nutrition/:page" exact component={Nutrition} />
                <Route path="/volunteer/:page" exact component={Volunteer} />
                <Route path="/search/:page" exact component={GlobalSearch} />

                <Route path="/kitchens/single/:id" component={KitchenSingle} />
                <Route path="/nutrition/single/:id" component={NutritionSingle} />
                <Route path="/volunteer/single/:id" component={VolunteerSingle} />

                 <Route path="/d3/Heatmap" exact component={US_Heatmap} />
                 <Route path="/d3/Income" exact component={Income_Histogram} />
                 <Route path="/d3/Calorie" exact component={Food_Scatter} />

                 <Route path="/developer/deaths" exact component={DeveloperDeaths} />
                 <Route path="/developer/sanitation" exact component={DeveloperSanitation} />
                 <Route path="/developer/water" exact component={DeveloperWater} />

            </Router>
        );
    }
}

export default App;
