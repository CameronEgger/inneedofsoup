function addProtocol(s) {
    if (!s.includes(':')) {
        s = 'http://' + s;
    }

    return s;
}

function formatMoney(x) {
    return '$' + x.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export { addProtocol, formatMoney };
