import React, { Component } from 'react';
import { KitchenCard, KITCHEN_SEARCH_FIELDS } from './KitchenCard';
import { NutritionCard, NUTRITION_SEARCH_FIELDS } from './NutritionCard';
import { VolunteerCard, VOLUNTEER_SEARCH_FIELDS } from './VolunteerCard';
import Pagination from './Pagination';
import SearchSort from './SearchSort';
import CardGrid from './CardGrid';
import { getQueryStringShort, getApiUrl } from './Api';

class GlobalSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kitchens: [],
            nutritions: [],
            volunteers: [],
            query: '',
            total_pages: 0
        };
    }

    getKitchenQueryString() {
        return getQueryStringShort(
            Object.keys(KITCHEN_SEARCH_FIELDS),
            this.state.query);
    }

    getNutritionQueryString() {
        return getQueryStringShort(
            Object.keys(NUTRITION_SEARCH_FIELDS),
            this.state.query);
    }

    getVolunteerQueryString() {
        return getQueryStringShort(
            Object.keys(VOLUNTEER_SEARCH_FIELDS),
            this.state.query);
    }

    fetchNewState(props) {
        var page = props.match.params.page ? props.match.params.page : 1;
        console.log(page);
        var kitchen_params = {
            page: page,
            results_per_page: 6,
            q: this.getKitchenQueryString()
        };

        this.setState(
            { total_pages: 0 },
            () => {
            fetch(getApiUrl('soup_kitchen', kitchen_params))
                .then(response => response.json())
                .then(data => this.setState((state, props) => {
                    return {
                        kitchens: data.objects,
                        total_pages: Math.max(state.total_pages, data.total_pages)
                    };
                }));

            var nutrition_params = {
                page: page,
                results_per_page: 6,
                q: this.getNutritionQueryString()
            };

            fetch(getApiUrl('food', nutrition_params))
                .then(response => response.json())
                .then(data => this.setState((state, props) => {
                    return {
                        nutritions: data.objects,
                        total_pages: Math.max(state.total_pages, data.total_pages)
                    };
                }));

            var volunteer_params = {
                page: page,
                results_per_page: 6,
                q: this.getVolunteerQueryString()
            };

            fetch(getApiUrl('donation_opportunity', volunteer_params))
                .then(response => response.json())
                .then(data => this.setState((state, props) => {
                    return {
                        volunteers: data.objects,
                        total_pages: Math.max(state.total_pages, data.total_pages)
                    };
                }));
        });
    }

    onNewQuery(query) {
        this.setState({ query: query },
            () => { this.fetchNewState(this.props); });
    }

    componentDidMount() {
        this.fetchNewState(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchNewState(props);
    }

    render() {
        var kitchen_cards = this.state.kitchens.map(instance =>
            <KitchenCard key={instance.ein} search_query={this.state.query}
                {...instance} />);

        var nutrition_cards = this.state.nutritions.map(instance =>
            <NutritionCard key={instance.food_id} search_query={this.state.query}
                {...instance} />);

        var volunteer_cards = this.state.volunteers.map(instance =>
            <VolunteerCard key={instance.ein} search_query={this.state.query}
                {...instance} />);

        var page = this.props.match.params.page ? parseInt(this.props.match.params.page) : 1;

        return (
            <div className="my-5 bg-light">
                <div className="container">
                    <SearchSort onNewQuery={(value) => this.onNewQuery(value)} />
                    <h2>Soup kitchens</h2>
                    <CardGrid cards={kitchen_cards} />
                    <h2>Nutrition</h2>
                    <CardGrid cards={nutrition_cards} />
                    <h2>Volunteering opportunities</h2>
                    <CardGrid cards={volunteer_cards} />
                    <Pagination page={page}
                        total_pages={this.state.total_pages}
                        base_url='/search/' />
                </div>
            </div>
        );
    }
}

export default GlobalSearch;
