import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './css/welcome.css';
import BackgroundSlideshow from 'react-background-slideshow'
import { Fade } from 'react-slideshow-image';
import vol from './Pictures/Volunteer2.jpg'
import nut from './Pictures/Food1.jpg'
import kit from './Pictures/Kitchen.jpg'
import vol1 from './Pictures/Volunteer1.jpg'
import nut1 from './Pictures/Nutrition1.jpg'
import kit1 from './Pictures/Kitchen1.jpg'

class Slide extends Component {
    render() {
        return (
            <div className="image-container">
                <img src={this.props.img} className="welcome-image" alt="" />
                <div className="middle-page">
                    <h1 className="text-transition">{this.props.text}</h1>
                    <Link className="btn btn-primary btn-lg my-5 welcome-button"
                            to={this.props.href} role="button">
                        Learn more
                    </Link>
                </div>
            </div>
        );
    }
}

class Welcome extends Component {
    componentDidMount() {
        document.getElementsByTagName('body')[0].className = 'no-scroll';
    }

    componentWillUnmount() {
        document.getElementsByTagName('body')[0].className = '';
    }
    render() {
        const fadeImages = [
            vol,
            nut,
            kit,
            vol1,
            nut1,
            kit1
        ];

        const fadeProperties = {
            duration: 5000,
            transitionDuration: 1000,
            infinite: true,
            scale: 1000,
            arrows: false
        };

        return (
            <Fade {...fadeProperties}>
                <Slide img={fadeImages[0]} href="/volunteer"
                    text="Volunteer in your community" />
                <Slide img={fadeImages[1]} href="/nutrition"
                    text="Live a healthier life" />
                <Slide img={fadeImages[2]} href="/kitchens"
                    text="Find a soup kitchen" />
                <Slide img={fadeImages[3]} href="/volunteer"
                    text="Volunteer in your community" />
                <Slide img={fadeImages[4]} href="/nutrition"
                    text="Live a healthier life" />
                <Slide img={fadeImages[5]} href="/kitchens"
                    text="Find a soup kitchen" />
            </Fade>
        );
    }
}

export default Welcome;
