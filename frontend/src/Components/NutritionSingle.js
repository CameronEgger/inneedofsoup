import React, { Component } from 'react';
import { getApiUrl } from './Api';
import { RelatedKitchens, RelatedVolunteers } from './RelatedGrid';
import { Link } from 'react-router-dom';

class NutritionSingle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            measure: null,
            grams_per_measure: 100.0,
            calories: 0.0,
            carbs: 0.0,
            fat: 0.0,
            protein: 0.0,
            fiber: 0.0,
            calories_per_100g: 0.0,
            carbs_per_100g: 0.0,
            fat_per_100g: 0.0,
            protein_per_100g: 0.0,
            fiber_per_100g: 0.0,
            img: null,
            related_kitchens: [],
            related_volunteers: [],
        };
    }

    componentDidMount() {
        fetch(getApiUrl('food/' + this.props.match.params.id))
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    render() {
        var name = (
            <h5 className="card-title">
                {this.state.name}
            </h5>
        );

        const { calories, carbs, fat, protein, fiber, grams_per_measure,
                calories_per_100g, carbs_per_100g, fat_per_100g, protein_per_100g,
                fiber_per_100g } = this.state;

        var rows = []

        rows.push((
            <tr key="0">
                <th>Calories</th>
                <td>{calories.toFixed()}</td>
                <td>{calories_per_100g.toFixed()}</td>
            </tr>
        ));

        rows.push((
            <tr key="1">
                <th>Carbs</th>
                <td>{carbs.toFixed()}</td>
                <td>{carbs_per_100g.toFixed()}</td>
            </tr>
        ));

        rows.push((
            <tr key="2">
                <th>Fat</th>
                <td>{fat.toFixed()}</td>
                <td>{fat_per_100g.toFixed()}</td>
            </tr>
        ));

        rows.push((
            <tr key="3">
                <th>Protein</th>
                <td>{protein.toFixed()}</td>
                <td>{protein_per_100g.toFixed()}</td>
            </tr>
        ));

        rows.push((
            <tr key="4">
                <th>Fiber</th>
                <td>{fiber.toFixed()}</td>
                <td>{fiber_per_100g.toFixed()}</td>
            </tr>
        ));

        var table = (
            <table className="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Per {this.state.measure}</th>
                        <th>Per 100g</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        );

        return (
            <div className="py-5 bg-light">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-9">
                            <div className="card">
                                <img className="card-img-top"
                                    src={this.state.img} alt="" />
                                <div className="card-body">
                                    {name}
                                    {table}
                                </div>
                            </div>
                        </div>
                    </div>
                    <RelatedKitchens kitchens={this.state.related_kitchens} />
                    <RelatedVolunteers volunteers={this.state.related_volunteers} />
                </div>
            </div>
        );
    }
}

export default NutritionSingle;
