import React, { Component } from 'react';
import * as d3 from "d3";
import * as topojson from "topojson";
import {getApiUrl} from './Api';
import { Link } from 'react-router-dom';
import D3Navbar from './D3Navbar';
import { withRouter } from 'react-router';
const income_id = "income";
const map_id = "US_map";
const scatter_id = "Scatter_stuff"
const TOTAL_PAGE = 5;
const state_to_idx = {
  "Alabama":"1",
  "Alaska":"2",
  "Arizona":"4",
  "Arkansas":"5",
  "California":"6",
  "Colorado":"8",
  "Connecticut":"9",
  "Delaware":"10",
  "District of Columbia":"11",
  "Florida":"12",
  "Georgia":"13",
  "Hawaii":"15",
  "Idaho":"16",
  "Illinois":"17",
  "Indiana":"18",
  "Iowa":"19",
  "Kansas":"20",
  "Kentucky":"21",
  "Louisiana":"22",
  "Maine":"23",
  "Maryland":"24",
  "Massachusetts":"25",
  "Michigan":"26",
  "Minnesota":"27",
  "Mississippi":"28",
  "Missouri":"29",
  "Montana":"30",
  "Nebraska":"31",
  "Nevada":"32",
  "New Hampshire":"33",
  "New Jersey":"34",
  "New Mexico":"35",
  "New York":"36",
  "North Carolina":"37",
  "North Dakota":"38",
  "Ohio":"39",
  "Oklahoma":"40",
  "Oregon":"41",
  "Pennsylvania":"42",
  "Rhode Island":"44",
  "South Carolina":"45",
  "South Dakota":"46",
  "Tennessee":"47",
  "Texas":"48",
  "Utah":"49",
  "Vermont":"50",
  "Virginia":"51",
  "Washington":"53",
  "West Virginia":"54",
  "Wisconsin":"55",
  "Wyoming":"56",
  "America Samoa":"60",
  "Federated States of Micronesia":"64",
  "Guam":"66",
  "Marshall Islands":"68",
  "Northern Mariana Islands":"69",
  "Palau":"70",
  "Puerto Rico":"72",
  "U.S. Minor Outlying Islands":"74",
  "Virgin Islands of the United States":"78"


}
class Food_Scatter extends Component{
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    var w = 600;
    var h = 400;
    var padding = 40;

    var dataset = [];
    const request = async (dataset) => {
      var temp = await fetch(getApiUrl('food', {page:1})).then(response => response.json())
            .then((response)=>{
        console.log(response);

        response.objects.forEach((value)=>{
          dataset.push([value.calories, value.carbs])
        });
      });
      temp = await fetch(getApiUrl('food', {page:2})).then(response => response.json())
            .then((response)=>{
              console.log(response);  
        response.objects.forEach((value)=>{
          dataset.push([value.calories, value.carbs])
        });
      });
      temp = await fetch(getApiUrl('food', {page:3})).then(response => response.json())
            .then((response)=>{
        response.objects.forEach((value)=>{
          dataset.push([value.calories, value.carbs])
        });
      });
      temp = await fetch(getApiUrl('food', {page:4})).then(response => response.json())
            .then((response)=>{
        response.objects.forEach((value)=>{
          dataset.push([value.calories, value.carbs])
        });
      });
      temp = await fetch(getApiUrl('food', {page:5})).then(response => response.json())
            .then((response)=>{
        response.objects.forEach((value)=>{
          dataset.push([value.calories, value.carbs])
        });
      });
      //scale function
      var xScale = d3.scaleLinear()
        //.domain(["Alabama","Alaska","Arizona","Arkansas","California"])
        .domain([0, d3.max(dataset, function(d) { return d[0]; })])
        //.range([padding, w-padding * 2]);
        .range([padding, w - padding * 2]);
        
      var yScale = d3.scaleLinear()
        .domain([0, d3.max(dataset, function(d) { return d[1]; })])
        //.range([padding, w-padding * 2]);
        .range([h - padding, padding]);
      
      var xAxis = d3.axisBottom().scale(xScale).ticks(5);
      
      var yAxis = d3.axisLeft().scale(yScale).ticks(5);
      
      //create svg element
      var svg = d3.select("#" + scatter_id)
            .append("svg")
            .attr("width", w)
            .attr("height", h);
            
      svg.selectAll("circle")
        .data(dataset)
        .enter()
        .append("circle")
        .attr("cx", function(d) {
          return xScale(d[0]);
        })
        .attr("cy", function(d) {
          return h - yScale(d[1]);
        })
        .attr("r", 5)
        .attr("fill", "green");
        
      //x axis
      svg.append("g")
        .attr("class", "x axis")  
        .attr("transform", "translate(0," + (h - padding) + ")")
        .call(xAxis);
      
      //y axis
      svg.append("g")
        .attr("class", "y axis")  
        .attr("transform", "translate(" + padding + ", 0)")
        .call(yAxis);
        
    }
    request(dataset);
    /*
    dataset = [{max: 46, min: 32},
          {max: 47, min: 31},
          {max: 51, min: 41},
          {max: 52, min: 38},
          {max: 38, min: 29}];
          */
    

    
  }
  render() {
    return (
      <div>
        <D3Navbar/>
        <div className="row">
        <h3 className = "col-12"> Scatter Plot of Calorie vs Carbs for Each Food</h3>
        <div id = {scatter_id} className = "col-12"></div>
        </div>
      </div>
    )
  }
}
export default Food_Scatter;
