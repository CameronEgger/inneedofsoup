import expect from 'expect';
import About from './About.js'
import React from 'react';
import { shallow, render, mount } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

describe("About page", () => {
  test("Only one About page loaded", () => {
  	const wrapper = shallow(<About />);
    expect(wrapper.length).toBe(1);
  });

  test("All 5 Authors cards are created", () => {
    const wrapper = mount(<About />);
    expect(wrapper.find('AuthorCard[name="Cameron Egger"]').length).toEqual(1);
    expect(wrapper.find('AuthorCard[name="David Lambert"]').length).toEqual(1);
    expect(wrapper.find('AuthorCard[name="Ethan Arnold"]').length).toEqual(1);
    expect(wrapper.find('AuthorCard[name="Henry Zhang"]').length).toEqual(1);
    expect(wrapper.find('AuthorCard[name="Tongrui Li"]').length).toEqual(1);
  });

  test("AuthorCards text is loaded", () => {
    const wrapper = mount(<About />);
    expect(wrapper.prop('text')).toBe(undefined);
  });
}); 



