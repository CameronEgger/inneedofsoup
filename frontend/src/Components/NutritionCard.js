import React, { Component } from 'react';
import { SearchContent } from './SearchUtil';
import { Link } from 'react-router-dom';

var NUTRITION_SEARCH_FIELDS = {
    'name': 'Name',
    'measure': 'Measure'
};

class NutritionCardRegular extends Component {
    render() {
        var kcal = this.props.calories_per_100g;
        var carbs = this.props.carbs_per_100g;
        var fat = this.props.fat_per_100g;
        var protein = this.props.protein_per_100g;
        var fiber = this.props.fiber_per_100g;

        kcal = kcal.toFixed();
        carbs = carbs.toFixed();
        fat = fat.toFixed();
        protein = protein.toFixed();
        fiber = fiber.toFixed();

        return (
            <>
                <p className="card-text">Per 100g:</p>
                <ul>
                    <li>{kcal} kcal</li>
                    <li>{carbs}g carbs</li>
                    <li>{fat}g fat</li>
                    <li>{protein}g protein</li>
                    <li>{fiber}g fiber</li>
                </ul>
            </>
       );
    }
}

class NutritionCard extends Component {
    render() {
        var content = this.props.search_query ?
            <SearchContent {...this.props} fields={NUTRITION_SEARCH_FIELDS} /> :
            <NutritionCardRegular {...this.props} />;
        return (
            <div className="card">
                <Link to={'/nutrition/single/' + this.props.food_id}>
                    <img className="card-img-top" src={this.props.img} alt="" />
                </Link>
                <div className="card-body">
                    <h5 className="card-title">
                        <Link to={'/nutrition/single/' + this.props.food_id}>
                            {this.props.name}
                        </Link>
                    </h5>
                    {content}
                </div>
            </div>
        );
    }
}

export { NutritionCard, NUTRITION_SEARCH_FIELDS };
