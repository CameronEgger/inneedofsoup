import React, { Component } from 'react';
import { addProtocol, formatMoney } from './Util';
import { SearchContent } from './SearchUtil';
import { Link } from 'react-router-dom';

var VOLUNTEER_SEARCH_FIELDS = {
    'name': 'Name',
    'address': 'Address',
    'city': 'City',
    'state': 'State',
    'zip_code': 'Zip code',
    'website': 'Website',
    'mission_statement': 'Mission statement'
};

class VolunteerCardRegular extends Component {
    render() {
        var extra_info = this.props.mission_statement ?
            this.props.mission_statement :
            (this.props.accepting_donations ?
                "Accepting donations!" :
                "Not accepting donations.");

        var website = "No website.";
        if (this.props.website) {
            website = (
                <a href={addProtocol(this.props.website)}>{this.props.website}</a>
            );
        }

        return (
            <>
                <p className="card-text">{this.props.city}, {this.props.state} {this.props.zip_code}</p>
                <p className="card-text">EIN: {this.props.ein}</p>
                <p className="card-text">{extra_info}</p>
                <p className="card-text">{website}</p>
                <p className="card-text">Assets: {formatMoney(this.props.assets)}</p>
                <p className="card-text">Income: {formatMoney(this.props.income)}</p>
            </>
        );
    }
}

class VolunteerCard extends Component {
    render() {
        var content = this.props.search_query ?
            <SearchContent {...this.props} fields={VOLUNTEER_SEARCH_FIELDS} /> :
            <VolunteerCardRegular {...this.props} />;
        return (
            <div className="card h-100">
                <Link to={'/volunteer/single/' + this.props.ein}>
                    <img className="card-img-top" src={this.props.img} alt="" />
                </Link>
                <div className="card-body">
                    <h5 className="card-title">
                        <Link to={'/volunteer/single/' + this.props.ein}>
                            {this.props.name}
                        </Link>
                    </h5>
                    {content}
                </div>
            </div>
        );
    }
}

export { VolunteerCard, VOLUNTEER_SEARCH_FIELDS };
