import React, { Component } from 'react';
import './bootstrap-4.3.1-dist/css/bootstrap.min.css';

import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

class NavbarItem extends Component {
    render() {
        return (
            <li className={"nav-item " + (this.props.active ? 'active' : '')}>
                <Link className="nav-link" to={this.props.href}>{this.props.name}</Link>
            </li>
        );
    }
}

class Navbar extends Component {
    matches(base, actual) {
        if (base === '/') return base === actual;
        return actual.startsWith(base);
    }

    render() {
        var items = [
            {href: "/", name: "Welcome"},
            {href: "/home", name: "Home"},
            {href: "/kitchens", name: "Soup Kitchens"},
            {href: "/nutrition", name: "Nutrition"},
            {href: "/volunteer", name: "Volunteer & Donate"},
            {href: "/about", name: "About"},
            {href: "/d3/Heatmap", name: "D3"},
            {href: "/developer/deaths", name: "Developer"},
            {href: "/search", name: "Search"}

        ];


        var navbar_items = items.map(x =>
            <NavbarItem key={x.href} active={this.matches(x.href, this.props.location.pathname) ? "1" : ""}
                href={x.href} name={x.name} />);

        return (
            <header>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark border-bottom border-warning">
                    <div className="navbar-brand" to="/">In Need Of Soup</div>
                    <button className="navbar-toggler" type="button"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            {navbar_items}
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}
export default withRouter(Navbar);
