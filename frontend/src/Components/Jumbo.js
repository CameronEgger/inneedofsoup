import React, { Component } from 'react';

class Jumbo extends Component {
    render() {
        return (
            <section className="jumbotron text-center">
                <div className="container">
                    <h1 className="jumbotron-heading">{this.props.text}</h1>
                </div>
            </section>
        );
    }
}

export default Jumbo;
