import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import './css/home.css'
import Kitchens from './Pictures/Kitchens.jpg';
import Nutrition from './Pictures/Nutrition.jpg';
import Volunteer from './Pictures/Volunteer.jpg';
import About from './Pictures/About.jpg';

class HomeCard extends Component {
    render() {
        return (
            <div className="card">
                <Link to={this.props.href}>
                    <img className="card-img-top" src={this.props.img} alt="" />
                </Link>
                <div className="card-body">
                    <h5 className="card-title">
                        <Link to={this.props.href}>
                            {this.props.title}
                        </Link>
                    </h5>
                    <p className="card-text">
                        {this.props.desc}
                    </p>
                </div>
            </div>
        );
    }
}

class Home extends Component {
    render() {
        return (
            <div className="my-5 bg-light">
                <div className="container">
                    <div className="row my-5">
                        <div className="col-sm-12 col-md-6">
                            <HomeCard href="/kitchens" title="Soup Kitchens"
                                desc="Find soup kitchens near you." img={Kitchens} />
                        </div>
                        <div className = "col-sm-12 col-md-6">
                            <HomeCard href="/nutrition" title="Nutrition"
                                desc="Learn about your dietary needs and the nutritional content of common foods." img={Nutrition} />
                        </div>
                    </div>
                    <div className = "row my-5">
                        <div className = "col-sm-12 col-md-6">   
                            <HomeCard href="/volunteer" title="Volunteer & Donate"
                                desc="Give back to the community by volunteering your time or money." img={Volunteer} />
                        </div>
                        <div className = "col-sm-12 col-md-6">
                            <HomeCard href="/about" title="About"
                                desc="Meet the creators of this website." img={About} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
