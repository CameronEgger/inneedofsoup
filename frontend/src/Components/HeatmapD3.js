import React, { Component } from 'react';
import * as d3 from "d3";
import * as topojson from "topojson";
import {getApiUrl} from './Api';
import { Link } from 'react-router-dom';
import D3Navbar from './D3Navbar';
import { withRouter } from 'react-router';
const income_id = "income";
const map_id = "US_map"
const TOTAL_PAGE = 5;
const state_to_idx = {
  "Alabama":"1",
  "Alaska":"2",
  "Arizona":"4",
  "Arkansas":"5",
  "California":"6",
  "Colorado":"8",
  "Connecticut":"9",
  "Delaware":"10",
  "District of Columbia":"11",
  "Florida":"12",
  "Georgia":"13",
  "Hawaii":"15",
  "Idaho":"16",
  "Illinois":"17",
  "Indiana":"18",
  "Iowa":"19",
  "Kansas":"20",
  "Kentucky":"21",
  "Louisiana":"22",
  "Maine":"23",
  "Maryland":"24",
  "Massachusetts":"25",
  "Michigan":"26",
  "Minnesota":"27",
  "Mississippi":"28",
  "Missouri":"29",
  "Montana":"30",
  "Nebraska":"31",
  "Nevada":"32",
  "New Hampshire":"33",
  "New Jersey":"34",
  "New Mexico":"35",
  "New York":"36",
  "North Carolina":"37",
  "North Dakota":"38",
  "Ohio":"39",
  "Oklahoma":"40",
  "Oregon":"41",
  "Pennsylvania":"42",
  "Rhode Island":"44",
  "South Carolina":"45",
  "South Dakota":"46",
  "Tennessee":"47",
  "Texas":"48",
  "Utah":"49",
  "Vermont":"50",
  "Virginia":"51",
  "Washington":"53",
  "West Virginia":"54",
  "Wisconsin":"55",
  "Wyoming":"56",
  "America Samoa":"60",
  "Federated States of Micronesia":"64",
  "Guam":"66",
  "Marshall Islands":"68",
  "Northern Mariana Islands":"69",
  "Palau":"70",
  "Puerto Rico":"72",
  "U.S. Minor Outlying Islands":"74",
  "Virgin Islands of the United States":"78"


}
class US_Heatmap extends Component{
  constructor(props) {
    super(props);
  }
  componentDidMount() {
  var margin = {top: 10, right: 30, bottom: 30, left: 40},
      width = 1000 - margin.left - margin.right,
      height = 1000 - margin.top - margin.bottom;

  // append the svg object to the body of the page
  var svg = d3.select("#"+map_id)
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom);
      console.log(svg);
  var unemployment = d3.map();
  var path = d3.geoPath();
  var x = d3.scaleLinear()
    .domain([0, 9])
    .rangeRound([600, 860]);
var color = d3.scaleThreshold()
    .domain(d3.range(0, 100))
    .range(d3.schemeBlues[9]);
    var g = svg.append("g")
    .attr("class", "key")
    .attr("transform", "translate(0,40)");
    g.selectAll("rect")
  .data(color.range().map(function(d) {
      d = color.invertExtent(d);
      if (d[0] == null) d[0] = x.domain()[0];
      if (d[1] == null) d[1] = x.domain()[1];
      return d;
    }))
  .enter().append("rect")
    .attr("height", 8)
    .attr("x", function(d) { return x(d[0]); })
    .attr("width", function(d) { return x(d[1]) - x(d[0]); })
    .attr("fill", function(d) { return color(d[0]); });
    g.append("text")
    .attr("class", "caption")
    .attr("x", x.range()[0])
    .attr("y", -6)
    .attr("fill", "#000")
    .attr("text-anchor", "start")
    .attr("font-weight", "bold")
    .text("Soup kitchen location");
    g.call(d3.axisBottom(x)
    .tickSize(13)
    .tickFormat(function(x, i) { return i ? x : x + "%"; })
    .tickValues(color.domain()))
  .select(".domain")
    .remove();
var data = []

const request = async (data) => {
    console.log("HELLO");
    var temp = await d3.json("https://d3js.org/us-10m.v1.json");
    console.log("D3");
    data.push(temp);
    temp = await fetch(getApiUrl('soup_kitchen', {page:1})).then(response => response.json());
    console.log("1 finished");
    data.push(temp);
    temp = await fetch(getApiUrl('soup_kitchen', {page:2})).then(response => response.json());
    data.push(temp);
    temp = await fetch(getApiUrl('soup_kitchen', {page:3})).then(response => response.json());
    data.push(temp);
    temp = await fetch(getApiUrl('soup_kitchen', {page:4})).then(response => response.json());
    data.push(temp);
    temp = await fetch(getApiUrl('soup_kitchen', {page:5})).then(response => response.json());
    data.push(temp);
  console.log("DATA");
  console.log(data);
  var us = data[0];
  data[1].objects.forEach((value)=>{
      console.log(typeof(value.zip_code));
      unemployment.set(state_to_idx[value.state], +5.2)
  });
  data[2].objects.forEach((value)=>{
      unemployment.set(state_to_idx[value.state], +10.0)
  });
  data[3].objects.forEach((value)=>{
      unemployment.set(state_to_idx[value.state], +23.0)
  });
  data[4].objects.forEach((value)=>{
      unemployment.set(state_to_idx[value.state], +10.0)
  });
  console.log(unemployment)
  svg.append("g")
      .attr("class", "counties")
    .selectAll("path")
    .data(topojson.feature(us, us.objects.states).features)
    .enter().append("path")
      .attr("fill", (d)=> {
        console.log(d)
        d.rate = unemployment.get(d.id.toString());
        console.log("HERE"+d.name)
        if (d.rate === undefined){
          d.rate = 1.0
        }

        return color(d.rate); })
      .attr("d", path)
    .append("title")
      .text(function(d) { return d.rate + "%"; });

  svg.append("path")
      .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
      .attr("class", "states")
      .attr("d", path);

}
request(data);

/*
var promises = [
  d3.json("https://d3js.org/us-10m.v1.json"),
  fetch(getApiUrl('soup_kitchen', {page:1})).then(response => response.json()),
  fetch(getApiUrl('soup_kitchen', {page:2})).then(response => response.json()),
  fetch(getApiUrl('soup_kitchen', {page:3})).then(response => response.json()),
  fetch(getApiUrl('soup_kitchen', {page:4})).then(response => response.json()),
  fetch(getApiUrl('soup_kitchen', {page:5})).then(response => response.json()),

]

Promise.all(promises).then((data)=>{
  console.log("DATA");
  console.log(data);
  var us = data[0];
  data[1].objects.forEach((value)=>{
      console.log(typeof(value.zip_code));
      unemployment.set(state_to_idx[value.state], +5.2)
  });
  data[2].objects.forEach((value)=>{
      unemployment.set(state_to_idx[value.state], +10.0)
  });
  data[3].objects.forEach((value)=>{
      unemployment.set(state_to_idx[value.state], +23.0)
  });
  data[4].objects.forEach((value)=>{
      unemployment.set(state_to_idx[value.state], +10.0)
  });
  console.log(unemployment)
  svg.append("g")
      .attr("class", "counties")
    .selectAll("path")
    .data(topojson.feature(us, us.objects.states).features)
    .enter().append("path")
      .attr("fill", (d)=> {
        console.log(d)
        d.rate = unemployment.get(d.id.toString());
        console.log("HERE"+d.name)
        if (d.rate === undefined){
          d.rate = 1.0
        }

        return color(d.rate); })
      .attr("d", path)
    .append("title")
      .text(function(d) { return d.rate + "%"; });

  svg.append("path")
      .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
      .attr("class", "states")
      .attr("d", path);

});
*/
  }
  render() {
    return (
      <div>
        <D3Navbar/>
        <div className="row">
        <h3 className = "col-12"> US Soup Kitchen heatmap</h3>
        <div id = {map_id} className = "col-12"></div>
        </div>
      </div>
    )
  }
}
export default US_Heatmap;
