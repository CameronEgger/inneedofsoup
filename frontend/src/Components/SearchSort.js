import React, { Component } from 'react';

class SearchSort extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: -1,
            ascending: false
        };
    }

    onButtonClick(idx) {
        this.setState((state, props) =>
            {
                if (state.active != idx) {
                    return { active: idx, ascending: true };
                } else if (!state.ascending) {
                    return { active: -1, ascending: false };
                } else {
                    return { active: idx, ascending: false };
                }
            }, () => {this.props.onNewSort(this.state.active,
                this.state.ascending)});
    }

    render() {
        var sort_buttons = '';
        if (this.props.sorts) {
            var buttons = []
            for (var i = 0; i < this.props.sorts.length; ++i) {
                var cls = this.state.active == i ? 
                    (this.state.ascending ? 'btn btn-success' : 'btn btn-danger') :
                    'btn btn-outline-secondary';
                buttons.push(<button className={cls} type="button" key={i}
                    onClick={this.onButtonClick.bind(this, i)} id={i}>{this.props.sorts[i]}</button>);
            }

            sort_buttons = (
                <div className="input-group-append">
                    <button className="btn btn-secondary" disabled>Sort by</button>
                    {buttons}
                </div>
            );
        }

        return (
            <div className="row my-5">
                <div className="col">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Search for..."
                            onChange={(event) => this.props.onNewQuery(event.target.value)} />
                        {sort_buttons}
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchSort;
