import React, { Component } from 'react';
import * as d3 from "d3";
import * as topojson from "topojson";
import './css/visualization.css';
import {getApiUrl} from './Api';
import { Link } from 'react-router-dom';
import './bootstrap-4.3.1-dist/css/bootstrap.min.css';

import { withRouter } from 'react-router';

class NavbarItem extends Component {
    render() {
        return (
            <li className={"nav-item " + (this.props.active ? 'active' : '')}>
                <Link className="nav-link" to={this.props.href}>{this.props.name}</Link>
            </li>
        );
    }
}
class DeveloperNavbar extends Component {        
  constructor(props) {
    super(props);
  }
  matches(base, actual) {
        if (base === '/') return base === actual;
        return actual.startsWith(base);
    }

  render() {
  	var items = [
            {href: "/developer/deaths", name: "Deaths"},
            {href: "/developer/sanitation", name: "Sanitation"},
            {href: "/developer/water", name: "Water"}
        ];


        var navbar_items = items.map(x =>
            <NavbarItem key={x.href} 
                href={x.href} name={x.name} />);
    return (
      <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark border-bottom border-warning">
                <div className="navbar-brand" to="/">Data Visualization</div>
                <button className="navbar-toggler" type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        {navbar_items}
                    </ul>
                </div>
            </nav>
        </div>
    )
  }
}
export default DeveloperNavbar;
