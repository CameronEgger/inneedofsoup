import React, { Component } from 'react';
import { getApiUrl } from './Api';
import { addProtocol, formatMoney } from './Util';
import { RelatedKitchens, RelatedFoods } from './RelatedGrid';
import GoogleMap from './GoogleMap';
import { Link } from 'react-router-dom';

class VolunteerSingle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            address: null,
            city: null,
            state: null,
            zip_code: null,
            website: null,
            mission_statement: null,
            img: null,
            deductibility: null,
            accepting_donations: null,
            assets: 0,
            income: 0,
            related_kitchens: [],
            related_foods: [],
        };
    }

    componentDidMount() {
        fetch(getApiUrl('donation_opportunity/' + this.props.match.params.id))
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    render() {
        var name = (
            <h5 className="card-title">
                {this.state.name}
            </h5>
        );
        var address1 = (
            <p className="card-text">
                {this.state.address}
            </p>
        );
        var address2 = (
            <p className="card-text">
                {this.state.city}, {this.state.state} {this.state.zip_code}
            </p>
        );
        var website = "No website.";
        if (this.state.website) {
            website = (
                <a href={addProtocol(this.state.website)}>{this.state.website}</a>
            );
        }

        var deductibility = (
            <p className="card-text">
                {(this.state.accepting_donations ? (
                    this.state.deductibility ? "This charity is accepting donations, and contributions are deductible." : "This charity is accepting donations, but contributions are not deductible.") : "This charity is not accepting donations.")}
            </p>
        );

        var mission = "No mission statement.";
        if (this.state.mission_statement) {
            mission = (
                <><b>Mission statement:</b> {this.state.mission_statement}</>
            );
        }

        var assets = (
            <p className="card-text">
                <b>Assets:</b> {formatMoney(this.state.assets)}
            </p>
        );

        var income = (
            <p className="card-text">
                <b>Income:</b> {formatMoney(this.state.income)}
            </p>
        );

        return (
            <div className="py-5 bg-light">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-7">
                            <div className="card">
                                <img className="card-img-top"
                                    src={this.state.img} alt="" />
                                <div className="card-body">
                                    {name}
                                    {address1}
                                    {address2}
                                    <p className="card-text">{website}</p>
                                    {deductibility}
                                    <p className="card-text">{mission}</p>
                                    {assets}
                                    {income}
                                </div>
                            </div>
                        </div>
                        <div className="col-5">
                            <GoogleMap latitude={this.state.latitude}
                                longitude={this.state.longitude} />
                        </div>
                    </div>
                    <RelatedKitchens kitchens={this.state.related_kitchens} />
                    <RelatedFoods foods={this.state.related_foods} />
                </div>
            </div>
        );
    }
}

export default VolunteerSingle;
