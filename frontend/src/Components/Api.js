
var BASE_URL = "http://localhost:8000/api/";
if (process.env.NODE_ENV === 'production') {
    BASE_URL = "https://api.inneedofsoup.xyz/api/";
}

function getApiUrl(endpoint, params) {
    var url = BASE_URL + endpoint;
    if (params) {
        var pairs = Object.entries(params);
        if (pairs.length >= 1) {
            url = url + '?' + pairs[0][0] + '=' + pairs[0][1];
        }
        for (var i = 1; i < pairs.length; ++i) {
            url = url + '&' + pairs[i][0] + '=' + pairs[i][1];
        }
    }
    return url;
}

function getDevApiUrl(endpoint, params) {
    var url = "https://api.globalantidote.me/" + endpoint;
    if (params) {
        var pairs = Object.entries(params);
        if (pairs.length >= 1) {
            url = url + '?' + pairs[0][0] + '=' + pairs[0][1];
        }
        for (var i = 1; i < pairs.length; ++i) {
            url = url + '&' + pairs[i][0] + '=' + pairs[i][1];
        }
    }
    return url;
}

function getQueryObject(query_fields, sort_fields, query, sort_idx, sort_ascending, extra_filters) {
    var res = [];
    query_fields.forEach((field) =>
        {res.push({name: field, op: 'ilike', val: '%' + query + '%'});});

    var filters = [{or: res}].concat(extra_filters);

    if (sort_idx == -1) {
        return {filters: filters};
    } else {
        return {
            filters: filters,
            order_by: [{
                field: sort_fields[sort_idx],
                direction: sort_ascending ? 'asc' : 'desc'
            }]
        };
    }
}

function getQueryString(query_fields, sort_fields, query, sort_idx, sort_ascending, extra_filters) {
    return JSON.stringify(getQueryObject(query_fields, sort_fields, query, sort_idx, sort_ascending, extra_filters));
}

function getQueryStringShort(query_fields, query) {
    return getQueryString(query_fields, [], query, -1, true, []);
}

export { getApiUrl, getDevApiUrl, getQueryString, getQueryStringShort };
