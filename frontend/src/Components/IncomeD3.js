import React, { Component } from 'react';
import * as d3 from "d3";
import * as topojson from "topojson";
import './css/visualization.css';
import {getApiUrl} from './Api';
import { Link } from 'react-router-dom';
import './bootstrap-4.3.1-dist/css/bootstrap.min.css';
import D3Navbar from './D3Navbar';
import { withRouter } from 'react-router';
import { BrowserRouter as Router, Route,  Switch } from "react-router-dom";
const income_id = "income";
const map_id = "US_map"
const TOTAL_PAGE = 5;
class Income_Histogram extends Component {        
  constructor(props) {
    super(props);
  }

  buildHistogram(data, id){ // Code modified from tutorial: https://www.d3-graph-gallery.com/graph/histogram_binSize.html

var margin = {top: 10, right: 30, bottom: 30, left: 40},
    width = 800 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// append the svg object to the body of the page
var svg = d3.select(id)
  .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");
     var x = d3.scaleLinear()
      .domain([d3.min(data), d3.max(data)])     // can use this instead of 1000 to have the max of data: d3.max(data, function(d) { return +d.price })
      .range([0, width]);

      svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));
     var y = d3.scaleLinear()
      .range([height, 0]);
        var yAxis = svg.append("g")
    var bins = d3.histogram().thresholds(x.ticks(10))(data)
    y.domain([0, d3.max(bins, function(d) { return d.length; })]);   // d3.hist has to be called before the Y axis obviously
    yAxis
        .transition()
        .duration(1000)
        .call(d3.axisLeft(y));
        var u = svg.selectAll("rect")
        .data(bins)
            u
        .enter()
        .append("rect") // Add a new rect for each new elements
        .merge(u) // get the already existing elements as well
        .transition() // and apply changes to all of them
        .duration(1000)
          .attr("x", 1)
          .attr("transform", function(d) { return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
          .attr("width", function(d) { return x(d.x1) - x(d.x0) -1 ; })
          .attr("height", function(d) { return height - y(d.length); })
          .style("fill", "#69b3a2")
  }

  buildIncomeHistogram(id){
    var data = [];

    const request = async (data, id) => {

      for(var i = 1; i <= TOTAL_PAGE; i++){
          var params = {
              page: i,
          };
          
          await fetch(getApiUrl('donation_opportunity', params))
            .then(response => response.json())
            .then((response)=>{
              response.objects.forEach((value)=>{
                if(value.income >0){
                  data.push(value.income);
                }
                
              });
              console.log(data);
              return 0;
            });
          }
        console.log("HERE");
        console.log(data);
        this.buildHistogram(data, id);
      }
    request(data, id);
  }
  componentDidMount() {
    this.buildIncomeHistogram("#"+income_id);
    this.forceUpdate();

  }

  render() {
    return (
      
      <div>
        <D3Navbar/>
        <div className="row">
        <h3 className = "col-12"> Histogram of income of charities</h3>
        <div id = {income_id} className = "col-12"></div>
        </div>
      </div>
    )
  }
}
export default Income_Histogram;