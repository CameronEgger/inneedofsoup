import expect from 'expect';
import NutritionSingle from './NutritionSingle.js'; //does this need to be capitalized?
import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });
describe("Nutrition single tests", () => {
  test("renders properly", () => {
    const wrapper = new NutritionSingle(null);
    expect(wrapper.state.calories).toEqual(0);
  }); 
  test("has related objects", () => {
    const wrapper = new NutritionSingle(null);
    expect(wrapper.state.related_kitchens).toEqual([]);
    expect(wrapper.state.related_volunteers).toEqual([]);
  }); 
  test("has updated components", () => {
    const wrapper = new NutritionSingle(null);
    expect(wrapper.state.calories).toEqual(0);
    expect(wrapper.state.calories_per_100g).toEqual(0);
    expect(wrapper.state.carbs).toEqual(0);
    expect(wrapper.state.carbs_per_100g).toEqual(0);
  }); 
});
