import expect from 'expect';
import About from './Jumbo.js'; //does this need to be capitalized?
import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

describe("Test loads Jumbo.js once", () => {
  test("Only one Jumbo page loaded", () => {
  	const wrapper = shallow(<About />);
    expect(wrapper.length).toBe(1);
  });

});

