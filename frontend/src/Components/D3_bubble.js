import * as d3 from "d3";
var node = document.createElement('div');

var data = [30, 86, 168, 281, 303, 365];


d3.select(node)
  .selectAll("div")
  .data(data)
    .enter()
    .append("div")
    .style("width", function(d) { return d + "px"; })
    .style("height", ()=>{return 30+"px;"})
    .text(function(d) { return d; });

export default node;
