import React, { Component } from 'react';

var BASE = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCd-oiASgJAI5PWxCP2Xc-EhwPltEsUbAI&q=';

class GoogleMap extends Component {
    render() {
        return (
            <iframe frameBorder="0" style={{height: '100%', width:'100%', border:0}}
                src={BASE + this.props.latitude + ',' + this.props.longitude}
                allowFullScreen>
            </iframe>
        );
    }
}

export default GoogleMap;
