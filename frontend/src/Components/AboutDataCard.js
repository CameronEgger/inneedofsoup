import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Link from '@material-ui/core/Link';

const styles = {
    card: {
      maxWidth: '100%',
    },
    media: {
      height: 225,
      width:200
    },
  };

const AboutDataCard = (props) => {
    return(
      <Link href={props.link} target="_blank" style={{'textDecoration':'none'}}>
        <CardActionArea>
          <Card style={styles.media}>
            <CardMedia src='img' style={{'paddingTop':'15px','textAlign':'center'}}>
              <img src={props.photo} alt={props.alt_text} style={{'height':'50px', 'width':'auto'}}/>
            </CardMedia>
            <CardContent>
              <Typography gutterBottom variant='h6' component="h3" style={{'marginBottom':'10px'}} align='center'>
                {props.name}
              </Typography>
              <Typography component="p" variant='caption' align="center" style={{'marginBottom':'10px'}}>
                {props.description}
              </Typography>
            </CardContent>
          </Card>
        </CardActionArea>
      </Link>
    )
}
AboutDataCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(AboutDataCard);
