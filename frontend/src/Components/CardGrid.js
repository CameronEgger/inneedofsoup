import React, { Component } from 'react';

class CardGrid extends Component {
    render() {
        const { cards } = this.props;
        var rows = [];
        for (var i = 0; i < Math.floor((cards.length + 2) / 3); ++i) {
            var cur = [];
            for (var j = 3 * i; j < Math.min(cards.length, 3 * i + 3); ++j) {
                cur.push(
                    <div className="col-md-4 col-sm-12" key={j}>
                        {cards[j]}
                    </div>
                );
            }
            rows.push(
                <div className="row my-5" key={i}>
                    {cur}
                </div>
            );
        }

        return rows;
    }
}

export default CardGrid;
