import expect from 'expect';
import About from './Welcome.js'; //does this need to be capitalized?
import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

describe("Test loads Welcome.js once", () => {
  test("Only one Welcome page loaded", () => {
  	const wrapper = shallow(<About />);
    expect(wrapper.length).toBe(1);
  });

});

