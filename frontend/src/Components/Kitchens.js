import React, { Component} from 'react';
import ReactDom from 'react-dom';
import Pagination from './Pagination';
import Jumbo from './Jumbo';
import CardGrid from './CardGrid';
import SearchSort from './SearchSort';
import { KitchenCard, KITCHEN_SEARCH_FIELDS } from './KitchenCard';
import { StateFilter, BinaryFilter, Filters } from './Filters';
import { getQueryString, getApiUrl } from './Api';

class Kitchens extends Component {
    constructor(props) {
        super(props);
        if (!this.props.match.params.page) {
            this.props.match.params.page = 1;
        }
        this.state = {
            objects: [],
            query: '',
            sort_idx: -1,
            sort_ascending: false,
            extra_filters: []
        };
        this.sort_names = ['Name', 'State'];
        this.sort_fields = ['name', 'state'];
    }

    getQueryString() {
        return getQueryString(
            Object.keys(KITCHEN_SEARCH_FIELDS),
            this.sort_fields,
            this.state.query,
            this.state.sort_idx,
            this.state.sort_ascending,
            this.state.extra_filters
        );
    }

    fetchNewState(props) {
        var params = {
            page: props.match.params.page,
            results_per_page: 9,
            q: this.getQueryString()
        };

        fetch(getApiUrl('soup_kitchen', params))
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    onNewQuery(query) {
        this.setState({ query: query },
            () => { this.fetchNewState(this.props); });
    }

    onNewSort(idx, ascending) {
        this.setState({ sort_idx: idx, sort_ascending: ascending },
            () => { this.fetchNewState(this.props); });
    }

    onFilterChange(filters) {
        this.setState({ extra_filters: filters },
            () => { this.fetchNewState(this.props); });
    }

    componentDidMount() {
        this.fetchNewState(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchNewState(props);
    }

    render() {
        var cards = this.state.objects.map(instance =>
            <KitchenCard key={instance.ein} search_query={this.state.query}
                {...instance} />);

        var filters = [
            new StateFilter('state'),
            new BinaryFilter('mission_statement', 'Mission statement', 'Required', 'neq', ''),
            new BinaryFilter('website', 'Website', 'Required', 'neq', '')
        ];

        return (
            <>
                <Jumbo text="Soup Kitchens" />
                <div className="my-5 bg-light">
                    <div className="container">
                        <SearchSort onNewQuery={(value) => this.onNewQuery(value)}
                            onNewSort={(idx, ascending) => this.onNewSort(idx, ascending)}
                            sorts={this.sort_names} />
                        <Filters onChange={(filters) => this.onFilterChange(filters)}
                            filters={filters} />
                        <CardGrid cards={cards} />
                        <Pagination page={this.state.page}
                            total_pages={this.state.total_pages}
                            base_url='/kitchens/' />
                    </div>
                </div>
            </>
        );
    }
}

export default Kitchens;
