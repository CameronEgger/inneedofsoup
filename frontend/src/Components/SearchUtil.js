import React, { Component } from 'react';

function getMatches(text, query) {
    text = text.toLowerCase();
    query = query.toLowerCase();
    var ans = [];
    for (var i = 0; i <= text.length - query.length; ++i) {
        if (text.substring(i, i + query.length) === query) {
            ans.push(i);
            i += query.length - 1;
        }
    }

    return ans;
}

function markIntervals(text, starts, stops) {
    var idx1 = 0;
    var idx2 = 0;
    var res = [];
    for (var i = 0; i <= text.length; ++i) {
        if (idx1 < starts.length && i == starts[idx1]) {
            res.push(0);
            idx1++;
        }
        if (idx2 < stops.length && i == stops[idx2]) {
            res.push(1);
            idx2++;
        }
        if (i < text.length) res.push(text[i]);
    }

    // have to do it this way because react doesn't like converting text to html
    // for xss reasons
    var res2 = [];
    var active = [];
    var in_mark = false;
    for (var i = 0; i < res.length; ++i) {
        if (res[i] === 0) {
            active = [];
            in_mark = true;
        } else if (res[i] === 1) {
            res2.push(
                <mark key={i}>
                    {active.join('')}
                </mark>
            );
            in_mark = false;
        } else if (in_mark) {
            active.push(res[i]);
        } else {
            res2.push(res[i]);
        }
    }

    return res2;
}

class SearchContent extends Component {
    render() {
        var items = [];
        Object.entries(this.props.fields).map((p) => {
            var field = p[0];
            var name = p[1];
            var value = this.props[field];
            
            var match_starts = getMatches(value, this.props.search_query);
            if (match_starts.length) {
                items.push(
                    <li key={field}>
                        {name}: {markIntervals(value, match_starts,
                            match_starts.map(i => i + this.props.search_query.length))}
                    </li>
                );
            }
        });

        return (
            <>
                <p className="card-text">Query matches:</p>
                <ul>
                    {items}
                </ul>
            </>
        );
    }
}

export { SearchContent };
