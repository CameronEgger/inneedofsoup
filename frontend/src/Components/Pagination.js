import React, { Component } from 'react';

import { Link } from 'react-router-dom';

class PageItem extends Component {
    render() {
        return (
            <li className={"page-item " + (this.props.disabled ?
                "disabled" : "")}>
                <Link className="page-link" to={this.props.base_url + this.props.page}>
                    {this.props.text}
                </Link>
            </li>
        );
    }
}

class Pagination extends Component {
    render() {
        var parts = [];
        //Attempt at doing a first button for pagination
        parts.push(<PageItem disabled={this.props.page == 1 ? "1" : ""}
            page={1} text="First" key="first"
            base_url={this.props.base_url} />);

        parts.push(<PageItem disabled={this.props.page == 1 ? "1" : ""}
            page={this.props.page - 1} text="Previous" key="0"
            base_url={this.props.base_url} />);
        for (var i = 1; i <= this.props.total_pages; ++i) {
            parts.push(<PageItem disabled={this.props.page == i ? "1" : ""}
                page={i} text={i} key={i} base_url={this.props.base_url} />);
        }
        parts.push(<PageItem disabled={this.props.page == this.props.total_pages ?
            "1" : ""} page={this.props.page + 1} text="Next" key="-1" 
            base_url={this.props.base_url} />);

        parts.push(<PageItem disabled={this.props.page == parts.length - 3 ? "1" : ""}
            page={parts.length - 3} text="Last" key="last"
            base_url={this.props.base_url} />);

        return (
            <nav>
                <ul className="pagination justify-content-center">
                    {parts}
                </ul>
            </nav>
        );
    }
}

export default Pagination;
