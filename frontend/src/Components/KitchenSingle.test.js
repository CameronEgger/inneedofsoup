import expect from 'expect';
import KitchenSingle from './KitchenSingle.js'; //does this need to be capitalized?
import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

it("testing kitchen integerties", ()=> {
    var temp = new KitchenSingle({
        match: {
            params: {
                id: 100
            }
        }
    });
    expect(temp.state == {
            name: "random stuff thats never true",
            address: null,
            city: null,
            state: null,
            zip_code: null,
            website: null,
            mission_statement: null,
            img: null,
    }).toEqual(false);
});

