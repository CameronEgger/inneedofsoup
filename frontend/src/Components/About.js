import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from './Card.js'


import Cameron from './Pictures/CameronEgger.jpg';
import Henry from './Pictures/Henry.JPG';
import Ethan from './Pictures/ethan.jpg';
import David from './Pictures/serious_photos_suck.jpeg';
import Li from './Pictures/TongruiLi.jpeg';

import './css/about.css';

import Jumbo from './Jumbo';

var GITLAB_URL = 'https://gitlab.com/api/v4/projects/11026291/';
var COMMITS_ENDPOINT = GITLAB_URL + 'repository/commits';
var ISSUES_ENDPOINT = GITLAB_URL + 'issues';

var PER_PAGE = 100;

class AboutSnippet extends Component {
    render() {
        return (
            <>
                <div className="lead">
                <h1>Background</h1>
                    The goal of this site is to provide a platform that will increase community engagement to create a better world by providing everyone with information on local soup kitchens, nutrition, and volunteer work. The goal is for everyone to come together to make a difference and help those in need. It also provides nutritional information to help people learn to eat well and be healthy.
                </div>
            </>
        );
    }
}

class AuthorStats extends Component {
    render() {
        const { commits, issues, tests } = this.props.stats;
        return (
            <ul>
                <li>{commits} commits</li>
                <li>{issues} issues closed</li>
                <li>{tests} unit tests created</li>
            </ul>
        );
    }
}

class AuthorCard extends Component {
    render() {
        return (
            <div className="col-md-4 col-sm-12 d-flex pb-3">
                <div className="card h-100">
                    <img className="card-img-top" src={this.props.img} alt="" />
                    <div className="card-body">
                        <h5 className="card-title">
                            {this.props.name}
                        </h5>
                        <p className="card-text">
                            {this.props.text}
                        </p>
                        <AuthorStats stats={this.props.stats} />
                    </div>
                </div>
            </div>
        );
    }
}

class AuthorSnippet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            commit_stats: {
                'Cameron Egger': 0,
                'David Lambert': 0,
                'Ethan Arnold': 0,
                'Henry Zhang': 0,
                'Tongrui Li': 0
            },
            issue_stats: {
                'Cameron Egger': 0,
                'David Lambert': 0,
                'Ethan Arnold': 0,
                'Henry Zhang': 0,
                'Tongrui Li': 0
            },
            test_stats: {
                'Cameron Egger': 8,
                'David Lambert': 15,
                'Ethan Arnold': 0,
                'Henry Zhang': 24,
                'Tongrui Li': 40
            }
        };

        this.author_usernames = {
            commit_stats: {
                'Cameron Egger': 'Cameron Egger',
                'Ethan Arnold': 'Ethan Arnold',
                'masonsbro456': 'Ethan Arnold',
                'David Lambert': 'David Lambert',
                'Henry Zhang': 'Henry Zhang',
                'Tongrui Li': 'Tongrui Li',
                'dlambert124': 'David Lambert'
            },
            issue_stats: {
                3447852: 'Cameron Egger',
                3450030: 'David Lambert',
                3463374: 'Ethan Arnold',
                3489261: 'Henry Zhang',
                3447800: 'Tongrui Li'
            }
        };
    }

    updateState(data, key) {
        for (var dat in data) {
            dat = data[dat];
            this.setState((state) => {
                var ans = {};
                for (var k in state) {
                    ans[k] = state[k];
                    if (k === key && (k === 'commit_stats' || dat.closed_by !== null)) {
                        var look = (k == 'commit_stats') ? dat.author_name : dat.closed_by.id;
                        look = this.author_usernames[k][look];
                        ans[k][look] += 1;
                    }
                }
                return ans;
            });
        }
    }

    fetchPage(endpoint, page, key) {
        fetch(endpoint + '?per_page=' + PER_PAGE + '&page=' + page)
        .then(res => res.json())
        .then(data => {
            this.updateState(data, key);
            if (data.length == PER_PAGE) {
                this.fetchPage(endpoint, page + 1, key);
            }
        });
    }

    componentDidMount() {
        this.fetchPage(COMMITS_ENDPOINT, 1, 'commit_stats');
        this.fetchPage(ISSUES_ENDPOINT, 1, 'issue_stats');
    }


    render() {
        var cameron_text = "I am a Junior who likes to spend time working on my competitive programming skills.";

        var cameron_stats = {
            commits: this.state.commit_stats['Cameron Egger'],
            issues: this.state.issue_stats['Cameron Egger'],
            tests: this.state.test_stats['Cameron Egger'],
        };

        var david_text = "I am a Senior and I enjoy taking my dog everywhere I can.";

        var david_stats = {
            commits: this.state.commit_stats['David Lambert'],
            issues: this.state.issue_stats['David Lambert'],
            tests: this.state.test_stats['David Lambert'],
        };

        var ethan_text = "I am a Senior who spends too much time doing competitive programming.";

        var ethan_stats = {
            commits: this.state.commit_stats['Ethan Arnold'],
            issues: this.state.issue_stats['Ethan Arnold'],
            tests: this.state.test_stats['Ethan Arnold'],
        };

        var henry_text = "I'm a Sophomore and you should ask to see a magic trick.";

        var henry_stats = {
            commits: this.state.commit_stats['Henry Zhang'],
            issues: this.state.issue_stats['Henry Zhang'],
            tests: this.state.test_stats['Henry Zhang'],
        };

        var li_text = "I'm a Junior who loves hardware as much as software.";

        var li_stats = {
            commits: this.state.commit_stats['Tongrui Li'],
            issues: this.state.issue_stats['Tongrui Li'],
            tests: this.state.test_stats['Tongrui Li'],
        };

        return (
            <>
                <div className="lead"><h1>Authors</h1></div>
                <div className="card-deck my-5 justify-content-center">
                    <AuthorCard name="Cameron Egger" img={Cameron} text={cameron_text} stats={cameron_stats} />
                    <AuthorCard name="David Lambert" img={David} text={david_text} stats={david_stats} />
                    <AuthorCard name="Ethan Arnold" img={Ethan} text={ethan_text} stats={ethan_stats} />
                </div>
                <div className="card-deck my-5 justify-content-center">
                    <AuthorCard name="Henry Zhang" img={Henry} text={henry_text} stats={henry_stats} />
                    <AuthorCard name="Tongrui Li" img={Li} text={li_text} stats={li_stats} />
                </div>
            </>
        );
    }
}

class ResourcesSnippet extends Component {
    render() {
        return (
            <>
                <div className="lead">
                <h1>Resources</h1>
                <span> </span>
                <span> </span>
                <span> </span>
                <span> </span>
                <Grid container spacing={40} justify = "center">
                  <Grid item>
                    <Card
                      name='Namecheap'
                      link='https://www.namecheap.com/'
                      photo={require('./Pictures/namecheap.png')}
                      alt_text='Namecheap Logo'
                      description="Provides services on domain name registration, and offers for sale domain names that are registered to third parties.">
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card
                      name='AWS'
                      link='https://aws.amazon.com/'
                      photo={require('./Pictures/amazon.png')}
                      alt_text='Namecheap Logo'
                      description="Provides on-demand cloud computing platforms.">
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card
                      name='Postman'
                      link='https://www.getpostman.com/'
                      photo={require('./Pictures/postman.png')}
                      alt_text='Namecheap Logo'
                      description="API Development Environment">
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card
                      name='Nginx'
                      link='https://www.nginx.com/products/nginx/'
                      photo={require('./Pictures/nginx.jpg')}
                      alt_text='Namecheap Logo'
                      description="Web server which can also be used as a reverse proxy, load balancer, mail proxy and HTTP cache.">
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card
                      name='Docker'
                      link='https://www.docker.com/'
                      photo={require('./Pictures/docker.png')}
                      alt_text='Namecheap Logo'
                      description="Computer program that performs operating-system-level virtualization.">
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card
                      name='PostgreSQL'
                      link='https://www.postgresql.org/'
                      photo={require('./Pictures/postgresql.png')}
                      alt_text='Namecheap Logo'
                      description="Open-source relational database management system.">
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card
                      name='Selenium'
                      link='https://www.seleniumhq.org/'
                      photo={require('./Pictures/seleniumhq.png')}
                      alt_text='Namecheap Logo'
                      description="Portable framework for testing web applications.">
                    </Card>
                  </Grid>
                </Grid>

                <h1>Links</h1>

                    <a href = "https://gitlab.com/CameronEgger/inneedofsoup">Gitlab </a><br/>
                    <a href = "https://documenter.getpostman.com/view/6781278/S11LsdCb"> Postman </a><br/>

                </div>
            </>
        );
    }
}

class About extends Component {
    render() {
        return (
            <>
                <Jumbo text="About" />
                <div className="my-5 bg-light">
                    <div className="container">
                        <AboutSnippet />
                        <AuthorSnippet />
                        <ResourcesSnippet />
                    </div>
                </div>
            </>
        );
    }
}

export default About;
