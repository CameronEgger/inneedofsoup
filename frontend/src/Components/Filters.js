import React, { Component } from 'react';

class StateFilterComponent extends Component {
    constructor(props) {
        super(props);
        this.states = [
            'Alabama',
            'Alaska',
            'Arizona',
            'Arkansas',
            'California',
            'Colorado',
            'Connecticut',
            'Delaware',
            'Florida',
            'Georgia',
            'Hawaii',
            'Idaho',
            'Illinois',
            'Indiana',
            'Iowa',
            'Kansas',
            'Kentucky',
            'Louisiana',
            'Maine',
            'Maryland',
            'Massachusetts',
            'Michigan',
            'Minnesota',
            'Mississippi',
            'Missouri',
            'Montana',
            'Nebraska',
            'Nevada',
            'New Hampshire',
            'New Jersey',
            'New Mexico',
            'New York',
            'North Carolina',
            'North Dakota',
            'Ohio',
            'Oklahoma',
            'Oregon',
            'Pennsylvania',
            'Rhode Island',
            'South Carolina',
            'South Dakota',
            'Tennessee',
            'Texas',
            'Utah',
            'Vermont',
            'Virginia',
            'Washington',
            'West Virginia',
            'Wisconsin',
            'Wyoming'
        ];
    }

    onChange(event) {
        if (event.target.selectedIndex === 0) {
            this.props.onChange([]);
        } else {
            this.props.onChange([{
                name: this.props.field,
                op: 'eq',
                val: this.states[event.target.selectedIndex - 1]
            }]);
        }
    }

    render() {
        var choices = [ 'Any state' ].concat(this.states);
        choices = choices.map(label => <option key={label}>{label}</option>);

        return (
            <div className="form-group row">
                <label className="col-form-label col-6">State</label>
                <select className="form-control col-6" id="stateSelect" onChange={(event) => this.onChange(event)} >
                    {choices}
                </select>
            </div>
        );
    }
}

class BinaryFilterComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activated: false
        };
    }

    onClick(event) {
        this.setState((state, props) => {
            return { activated: !state.activated };
        }, () => {
            if (this.state.activated) {
                this.props.onChange([{
                    name: this.props.field,
                    op: this.props.op,
                    val: this.props.val
                }]);
            } else {
                this.props.onChange([]);
            }
        });
    }

    render() {
        var cls = this.state.activated ? 'btn-primary' : 'btn-outline-primary';
        return (
            <div className="form-group row">
                <label className="col-form-label col-6">{this.props.label}</label>
                <button className={'btn col-6 ' + cls} onClick={(event) => {event.preventDefault(); this.onClick(event)}}>
                    {this.props.button}
                </button>
            </div>
        );
    }
}

class NumericalFilterComponent extends Component {
    onChange(event) {
        var idx = event.target.selectedIndex; // any, < 0, 0 <= 1, 1 <= 2, 2 <
        if (idx === 0) {
            this.props.onChange([]);
        } else {
            var res = [];
            if (idx > 1) {
                res.push({
                    name: this.props.field,
                    op: '>=',
                    val: this.props.thresholds[idx - 2]
                });
            }
            if (idx < this.props.thresholds.length + 1) {
                res.push({
                    name: this.props.field,
                    op: '<',
                    val: this.props.thresholds[idx - 1]
                });
            }
            this.props.onChange(res);
        }
    }

    render() {
        var choices = [ 'Any', '(-inf, ' + this.props.thresholds[0] + ')' ];
        for (var i = 0; i < this.props.thresholds.length - 1; ++i) {
            choices.push('[' + this.props.thresholds[i] + ', ' + this.props.thresholds[i + 1] + ')');
        }
        choices.push('[' + this.props.thresholds[this.props.thresholds.length - 1] + ', inf)');

        choices = choices.map((label, i) => <option key={i}>{label}</option>);

        return (
            <div className="form-group row">
                <label className="col-form-label col-6">{this.props.label}</label>
                <select className="form-control col-6" id={this.props.field + 'Select'}
                        onChange={(event) => this.onChange(event)}>
                    {choices}
                </select>
            </div>
        );
    }
}

class StateFilter {
    constructor(field) {
        this.field = field;
    }

    create(key, cb) {
        return <StateFilterComponent field={this.field} onChange={cb} key={key} />
    }
}

class BinaryFilter {
    constructor(field, label, button, op, val) {
        this.field = field;
        this.label = label;
        this.button = button;
        this.op = op;
        this.val = val;
    }

    create(key, cb) {
        return <BinaryFilterComponent field={this.field} label={this.label}
            button={this.button} op={this.op} val={this.val} onChange={cb}
            key={key} />
    }
}

class NumericalFilter {
    constructor(field, label, thresholds) {
        this.field = field;
        this.label = label;
        this.thresholds = thresholds;
    }

    create(key, cb) {
        return <NumericalFilterComponent field={this.field} label={this.label}
            thresholds={this.thresholds} onChange={cb} key={key} />
    }
}

class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filters_showing: false,
            filter_states: this.props.filters.map(filter => [])
        };
    }

    toggleShowFilters() {
        this.setState((state, props) => {
            return { filters_showing: !state.filters_showing };
        });
    }

    onFilterChange(idx, val) {
        this.setState((state, props) => {
            var filter_states = state.filter_states;
            filter_states[idx] = val;
            return { filter_states: filter_states };
        }, () => {
            this.props.onChange(this.state.filter_states.flat());
        });
    }

    render() {
        var maybe_filters = this.state.filters_showing
            ? (
                <div className="col-6">
                    <form>
                        {this.props.filters.map((filter, idx) =>
                            filter.create(idx, this.onFilterChange.bind(this, idx)))}
                    </form>
                </div>
            ) : <></>;
        return (
            <>
                {maybe_filters}
                <a href="" onClick={(e) => {e.preventDefault(); this.toggleShowFilters();}}>
                    {this.state.filters_showing ? 'hide filters' : 'show filters'}
                </a>
            </>
        );
    }
}

export { StateFilter, BinaryFilter, NumericalFilter, Filters };
