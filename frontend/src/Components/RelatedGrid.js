import React, { Component } from 'react';
import { getApiUrl } from './Api';
import CardGrid from './CardGrid';
import { KitchenCard } from './KitchenCard';
import { NutritionCard } from './NutritionCard';
import { VolunteerCard } from './VolunteerCard';

var MAX_RELATED = 3;

class RelatedItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        };
    }

    componentWillReceiveProps(props) {
        props.pairs.slice(0, MAX_RELATED).map(pair => {
            if (!(pair[props.key_name] in this.state.data)) {
                fetch(getApiUrl(props.url + '/' + pair[props.key_name]))
                    .then(response => response.json())
                    .then(dat => this.setState((state, props) => {
                        var res = state.data;
                        res[pair[props.key_name]] = dat;
                        return { data: res };
                    }));
            }
        });
    }

    render() {
        const CardType = this.props.card;
        var cards = Object.values(this.state.data).map((dat, i) =>
            <CardType key={i} {...dat} />);

        return (
            <>
                <div className="row my-5 justify-content-center">
                    <h2>{this.props.title}</h2>
                </div>
                <CardGrid cards={cards} />
            </>
        );
    }
}

class RelatedKitchens extends Component {
    render() {
        return (
            <RelatedItems key_name="soup_kitchen_ein" url="soup_kitchen"
                card={KitchenCard} title="Related soup kitchens"
                pairs={this.props.kitchens} />
        );
    }
}

class RelatedFoods extends Component {
    render() {
        return (
            <RelatedItems key_name="food_id" url="food"
                card={NutritionCard} title="Related foods"
                pairs={this.props.foods} />
        );
    }
}

class RelatedVolunteers extends Component {
    render() {
        return (
            <RelatedItems key_name="volunteer_ein" url="donation_opportunity"
                card={VolunteerCard} title="Related volunteering opportunities"
                pairs={this.props.volunteers} />
        );
    }
}

export { RelatedKitchens, RelatedFoods, RelatedVolunteers };
