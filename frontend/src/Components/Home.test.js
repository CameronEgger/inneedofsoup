import expect from 'expect';
import Home from './Home.js'; //does this need to be capitalized?
import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

describe("Test loads Home.js once", () => {
  test("Only one Home page loaded", () => {
  	const wrapper = shallow(<Home />);
    expect(wrapper.length).toBe(1);
  });

});

