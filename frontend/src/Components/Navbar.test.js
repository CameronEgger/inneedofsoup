import expect from 'expect';
import React from 'react';
import Navbar from './Navbar.js';
import { shallow, mount } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

describe("Test loads Navbar.js once", () => {
  test("Only one Navbar page loaded", () => {
  	const wrapper = shallow(<Navbar />);
    expect(wrapper.length).toBe(1);
  });

  test("Test if NavBar wrapper returns fn", () => {
   var wrapper = new Navbar.WrappedComponent();
   expect(typeof wrapper.render).toEqual("function");
  });
}); 

