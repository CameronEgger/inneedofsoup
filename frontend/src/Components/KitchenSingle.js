import React, { Component } from 'react';
import { getApiUrl } from './Api';
import { addProtocol } from './Util';
import GoogleMap from './GoogleMap';
import NutritionCard from './NutritionCard';
import VolunteerCard from './VolunteerCard';
import CardGrid from './CardGrid';
import { RelatedFoods, RelatedVolunteers } from './RelatedGrid';
import { Link } from 'react-router-dom';

class KitchenSingle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            address: null,
            city: null,
            state: null,
            zip_code: null,
            website: null,
            mission_statement: null,
            img: null,
            latitude: 0.0,
            longitude: 0.0,
            related_foods: [],
            related_volunteers: [],
        };
    }

    componentDidMount() {
        fetch(getApiUrl('soup_kitchen/' + this.props.match.params.id))
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    render() {
        var name = (
            <h5 className="card-title">
                {this.state.name}
            </h5>
        );
        var address1 = (
            <p className="card-text">
                {this.state.address}
            </p>
        );
        var address2 = (
            <p className="card-text">
                {this.state.city}, {this.state.state} {this.state.zip_code}
            </p>
        );
        var website = "No website.";
        if (this.state.website) {
            website = (
                <a href={addProtocol(this.state.website)}>{this.state.website}</a>
            );
        }

        var mission = "No mission statement.";
        if (this.state.mission_statement) {
            mission = (
                <><b>Mission statement:</b> {this.state.mission_statement}</>
            );
        }

        return (
            <div className="py-5 bg-light">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-7">
                            <div className="card">
                                <img className="card-img-top"
                                    src={this.state.img} alt="" />
                                <div className="card-body">
                                    {name}
                                    {address1}
                                    {address2}
                                    <p className="card-text">{website}</p>
                                    <p className="card-text">{mission}</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-5">
                            <GoogleMap latitude={this.state.latitude}
                                longitude={this.state.longitude} />
                        </div>
                    </div>
                    <RelatedFoods foods={this.state.related_foods} />
                    <RelatedVolunteers volunteers={this.state.related_volunteers} />
                </div>
            </div>
        );
    }
}

export default KitchenSingle;
