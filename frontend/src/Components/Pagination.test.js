import expect from 'expect';
import Pagination from './Pagination.js'; //does this need to be capitalized?
import React from 'react';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 
Enzyme.configure({ adapter: new Adapter() });

describe("Test loads Pagination.js once", () => {
  test("Only one Pagination page loaded", () => {
  	const wrapper = shallow(<Pagination />);
    expect(wrapper.length).toBe(1);
  });
});
