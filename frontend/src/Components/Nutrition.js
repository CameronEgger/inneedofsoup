import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Pagination from './Pagination';
import Jumbo from './Jumbo';
import CardGrid from './CardGrid';
import SearchSort from './SearchSort';
import { NutritionCard, NUTRITION_SEARCH_FIELDS } from './NutritionCard';
import { NumericalFilter, Filters } from './Filters';
import { getQueryString, getApiUrl } from './Api';

class Nutrition extends Component {
    constructor(props) {
        super(props);
        if (!this.props.match.params.page) {
            this.props.match.params.page = 1;
        }
        this.state = {
            objects: [],
            query: '',
            sort_idx: -1,
            sort_ascending: false,
            extra_filters: []
        };
        this.sort_names = ['Name', 'Calories', 'Carbs', 'Fat', 'Protein', 'Fiber'];
        this.sort_fields = ['name', 'calories_per_100g', 'carbs_per_100g', 'fat_per_100g', 'protein_per_100g', 'fiber_per_100g'];
    }

    getQueryString() {
        return getQueryString(
            Object.keys(NUTRITION_SEARCH_FIELDS),
            this.sort_fields,
            this.state.query,
            this.state.sort_idx,
            this.state.sort_ascending,
            this.state.extra_filters
        );
    }

    fetchNewState(props) {
        var params = {
            page: props.match.params.page,
            results_per_page: 9,
            q: this.getQueryString()
        };

        fetch(getApiUrl('food', params))
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    onNewQuery(query) {
        this.setState({ query: query },
            () => { this.fetchNewState(this.props); });
    }

    onNewSort(idx, ascending) {
        this.setState({ sort_idx: idx, sort_ascending: ascending },
            () => { this.fetchNewState(this.props); });
    }

    onFilterChange(filters) {
        this.setState({ extra_filters: filters },
            () => { this.fetchNewState(this.props); });
    }

    componentDidMount() {
        this.fetchNewState(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchNewState(props);
    }

    render() {
        var cards = this.state.objects.map(instance =>
            <NutritionCard key={instance.food_id} search_query={this.state.query}
                {...instance} />);

        var filters = [
            new NumericalFilter('calories_per_100g', 'Calories', [50, 100, 150, 200]),
            new NumericalFilter('carbs_per_100g', 'Carbs', [5, 10, 15, 20, 25]),
            new NumericalFilter('fat_per_100g', 'Fat', [5, 10, 15, 20, 25]),
            new NumericalFilter('protein_per_100g', 'Protein', [5, 10, 15, 20, 25]),
            new NumericalFilter('fiber_per_100g', 'Fiber', [5, 10, 15, 20, 25])
        ];

        return (
            <>
                <Jumbo text="Nutritional Information" />
                <div className="my-5 bg-light">
                    <div className="container">
                        <SearchSort onNewQuery={(value) => this.onNewQuery(value)}
                            onNewSort={(idx, ascending) => this.onNewSort(idx, ascending)}
                            sorts={this.sort_names} />
                        <Filters onChange={(filters) => this.onFilterChange(filters)}
                            filters={filters} />
                        <CardGrid cards={cards} />
                        <Pagination page={this.state.page}
                            total_pages={this.state.total_pages}
                            base_url='/nutrition/' />
                    </div>
                </div>
            </>
        );
    }
}

export default Nutrition;
