import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Pagination from './Pagination';
import Jumbo from './Jumbo';
import { getQueryString, getApiUrl } from './Api';
import { StateFilter, BinaryFilter, NumericalFilter, Filters } from './Filters';
import CardGrid from './CardGrid';
import SearchSort from './SearchSort';
import { VolunteerCard, VOLUNTEER_SEARCH_FIELDS } from './VolunteerCard';
import './css/grid.css'

class Volunteer extends Component {
    constructor(props) {
        super(props);
        if (!this.props.match.params.page) {
            this.props.match.params.page = 1;
        }
        this.state = {
            objects: [],
            query: '',
            sort_idx: -1,
            sort_ascending: false,
            extra_filters: []
        };
        this.sort_names = ['Name', 'Assets', 'Income'];
        this.sort_fields = ['name', 'assets', 'income'];
    }

    getQueryString() {
        return getQueryString(
            Object.keys(VOLUNTEER_SEARCH_FIELDS),
            this.sort_fields,
            this.state.query,
            this.state.sort_idx,
            this.state.sort_ascending,
            this.state.extra_filters
        );
    }

    fetchNewState(props) {
        var params = {
            page: props.match.params.page,
            results_per_page: 9,
            q: this.getQueryString()
        };

        fetch(getApiUrl('donation_opportunity', params))
            .then(response => response.json())
            .then(data => this.setState(data));
    }

    onNewQuery(query) {
        this.setState({ query: query },
            () => { this.fetchNewState(this.props); });
    }

    onNewSort(idx, ascending) {
        this.setState({ sort_idx: idx, sort_ascending: ascending },
            () => { this.fetchNewState(this.props); });
    }

    onFilterChange(filters) {
        this.setState({ extra_filters: filters },
            () => { this.fetchNewState(this.props); });
    }

    componentDidMount() {
        this.fetchNewState(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchNewState(props);
    }

    render() {
        var cards = this.state.objects.map(instance =>
            <VolunteerCard key={instance.ein} search_query={this.state.query}
                {...instance} />);

        var filters = [
            new StateFilter('state'),
            new BinaryFilter('mission_statement', 'Mission statement', 'Required', 'neq', ''),
            new BinaryFilter('website', 'Website', 'Required', 'neq', ''),
            new BinaryFilter('accepting_donations', 'Accepting donations', 'Required', 'eq', '1'),
            new BinaryFilter('deductibility', 'Deductibility status', 'Required', 'neq', ''),
            new NumericalFilter('assets', 'Assets', [10000, 100000, 1000000]),
            new NumericalFilter('income', 'Income', [10000, 100000, 1000000])
        ];

        return (
            <>
                <Jumbo text="Volunteer & Donate" />
                <div className="my-5 bg-light">
                    <div className="container">
                        <SearchSort onNewQuery={(value) => this.onNewQuery(value)}
                            onNewSort={(idx, ascending) => this.onNewSort(idx, ascending)}
                            sorts={this.sort_names} />
                        <Filters onChange={(filters) => this.onFilterChange(filters)}
                            filters={filters} />
                        <CardGrid cards={cards} />
                        <Pagination page={this.state.page}
                            total_pages={this.state.total_pages}
                            base_url='/volunteer/' />
                    </div>
                </div>
            </>
        );
    }
}

export default Volunteer;
