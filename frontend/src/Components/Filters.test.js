import expect from 'expect';
import { StateFilter, BinaryFilter, NumericalFilter, Filters } from './Filters.js'
import React from 'react';
import { shallow, render, mount } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("State filter tests", () => {
  test("All state exists", () => {
    const wrapper =new StateFilter();
    const wrapper_comp = wrapper.create(null, null);
    const states = (new wrapper_comp.type()).states;
    expect(states.length).toEqual(50);
  }); 
});

describe("Binary filter tests", () => {

  test("Button exists", () => {
    var wrapper =new BinaryFilter();
    wrapper = wrapper.create(null, null);
    wrapper = (new wrapper.type());
    const rend = shallow(<wrapper/>);
    expect(rend.find("button")).toBeDefined();  
  }); 
  test("Activation defined", () => {
    var wrapper =new BinaryFilter();
    wrapper = wrapper.create(null, null);
    wrapper = (new wrapper.type());
    expect(wrapper.state.activated).toBeDefined();  
  });
});

describe("Numeric filter tests", () => {
    test("Filter exists", () => {
        var wrapper =new NumericalFilter();
        wrapper = wrapper.create(null, null);
        wrapper = (new wrapper.type());
        const rend = shallow(<wrapper/>);
        expect(rend).toBeDefined(); 
    });
});





