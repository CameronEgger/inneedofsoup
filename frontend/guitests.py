import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class Tests(unittest.TestCase):
    url = "https://inneedofsoup.xyz/"
    about_url = "https://inneedofsoup.xyz/about"
    kitchens_url = "https://inneedofsoup.xyz/kitchens"
    nutrition_url = "https://inneedofsoup.xyz/nutrition"
    volunteer_url = "https://inneedofsoup.xyz/volunteer"
    home_url = "https://inneedofsoup.xyz/home"

    # def test_chrome_working(self):
    #     browser = webdriver.Chrome("./chromedriver")
    #     browser.get(self.url)
    #     time.sleep(3)
    #     browser.quit()
    #
    # def test_firefox_working(self):
    #     browser = webdriver.Firefox(executable_path="./geckodriver")
    #     browser.get(self.url)
    #     time.sleep(3)
    #     browser.quit()

    def setUp(self):
        self.driver = webdriver.Chrome("./chromedriver")
        self.driver.get("https://inneedofsoup.xyz/")
        self.driver.title

    # def test_home_title(self):
    #     self.driver.get(self.url)
    #     self.assertIn("In Need Of Soup", self.driver.title)
    #
    # def test_about_title(self):
    #     self.driver.get(self.about_url)
    #     self.assertIn("In Need Of Soup", self.driver.title)
    #
    # def test_kitchens_title(self):
    #     self.driver.get(self.kitchens_url)
    #     self.assertIn("In Need Of Soup", self.driver.title)
    #
    # def test_nutrition_title(self):
    #     self.driver.get(self.nutrition_url)
    #     self.assertIn("In Need Of Soup", self.driver.title)
    #
    # def test_volunteer_title(self):
    #     self.driver.get(self.volunteer_url)
    #     self.assertIn("In Need Of Soup", self.driver.title)
    #
    # def test_home_navbar(self):
    #     self.driver.get(self.url)
    #     nav = self.driver.find_element_by_tag_name("nav")
    #     self.assertIsNotNone(nav)
    #
    # def test_about_navbar(self):
    #     self.driver.get(self.about_url)
    #     nav = self.driver.find_element_by_tag_name("nav")
    #     self.assertIsNotNone(nav)
    #
    # def test_kitchen_navbar(self):
    #     self.driver.get(self.kitchens_url)
    #     nav = self.driver.find_element_by_tag_name("nav")
    #     self.assertIsNotNone(nav)
    #
    # def test_nutrition_navbar(self):
    #     self.driver.get(self.nutrition_url)
    #     nav = self.driver.find_element_by_tag_name("nav")
    #     self.assertIsNotNone(nav)
    #
    # def test_volunteer_navbar(self):
    #     self.driver.get(self.volunteer_url)
    #     nav = self.driver.find_element_by_tag_name("nav")
    #     self.assertIsNotNone(nav)
    #
    # def test_about_link(self):
    #     self.driver.get(self.home_url)
    #     about_link = self.driver.find_element_by_link_text("About")
    #     about_link.click()
    #     self.assertIn(self.about_url, self.driver.current_url)
    #
    # def test_kitchen_link(self):
    #     self.driver.get(self.home_url)
    #     kitchen_link = self.driver.find_element_by_link_text("Soup Kitchens")
    #     kitchen_link.click()
    #     self.assertIn(self.kitchens_url, self.driver.current_url)
    #
    # def test_nutrition_link(self):
    #     self.driver.get(self.home_url)
    #     nutrition_link = self.driver.find_element_by_link_text("Nutrition")
    #     nutrition_link.click()
    #     self.assertIn(self.nutrition_url, self.driver.current_url)
    #
    # def test_volunteer_link(self):
    #     self.driver.get(self.home_url)
    #     volunteer_link = self.driver.find_element_by_link_text("Volunteer & Donate")
    #     volunteer_link.click()
    #     self.assertIn(self.volunteer_url, self.driver.current_url)

    def test_gitlab_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("Gitlab").click()
        self.assertIn(
            "https://gitlab.com/CameronEgger/inneedofsoup", self.driver.current_url
        )

    def test_namecheap_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("Namecheap").click()
        self.assertIn("https://www.namecheap.com/", self.driver.current_url)

    def test_aws_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("AWS").click()
        self.assertIn("https://aws.amazon.com/", self.driver.current_url)

    def test_postman_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("Postman").click()
        self.assertIn("https://www.getpostman.com/", self.driver.current_url)

    def test_nginx_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("Nginx").click()
        self.assertIn("https://www.nginx.com/products/nginx/", self.driver.current_url)

    def test_docker_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("Docker").click()
        self.assertIn("https://www.docker.com/", self.driver.current_url)

    def test_postgresql_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("PostgreSQL").click()
        self.assertIn("https://www.postgresql.org/", self.driver.current_url)

    def test_selenium_link(self):
        self.driver.get(self.home_url)
        about_link = self.driver.find_element_by_link_text("About")
        about_link.click()
        self.driver.find_element_by_link_text("Selenium").click()
        self.assertIn("https://www.seleniumhq.org/", self.driver.current_url)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
