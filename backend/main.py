import os

import flask
import flask_sqlalchemy
import flask_restless

from sqlalchemy.orm import relationship

import config

from flask import Flask, render_template
import sys

app = Flask(
    __name__,
    static_folder="../front_end/static",
    template_folder="../front_end/templates",
)

app.config["SQLALCHEMY_DATABASE_URI"] = config.get_key("sql")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_POOL_SIZE"] = 2

db = flask_sqlalchemy.SQLAlchemy(app)


def get_many_preprocessor(**kw):
    # This checks if the preprocessor function is being called before a
    # request that does not have search parameters.

    # Create the filter you wish to add; in this case, we include only
    # instances with ``id`` not equal to 1.
    print("HAERAWERAWS", file=sys.stderr)
    print(kw, file=sys.stderr)
    return


class KitchenFoodAssociation(db.Model):
    soup_kitchen_ein = db.Column(
        db.Integer, db.ForeignKey("soup_kitchen.ein"), primary_key=True
    )
    food_id = db.Column(db.Integer, db.ForeignKey("food.food_id"), primary_key=True)


class KitchenVolunteerAssociation(db.Model):
    soup_kitchen_ein = db.Column(
        db.Integer, db.ForeignKey("soup_kitchen.ein"), primary_key=True
    )
    volunteer_ein = db.Column(
        db.Integer, db.ForeignKey("donation_opportunity.ein"), primary_key=True
    )


class FoodVolunteerAssociation(db.Model):
    food_id = db.Column(db.Integer, db.ForeignKey("food.food_id"), primary_key=True)
    volunteer_ein = db.Column(
        db.Integer, db.ForeignKey("donation_opportunity.ein"), primary_key=True
    )


class SoupKitchen(db.Model):
    ein = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)
    address = db.Column(db.Unicode)
    city = db.Column(db.Unicode)
    state = db.Column(db.Unicode)
    zip_code = db.Column(db.Unicode)
    # phone = db.Column(db.Unicode)
    website = db.Column(db.Unicode)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    mission_statement = db.Column(db.Text)
    img = db.Column(db.Unicode)

    related_foods = relationship(KitchenFoodAssociation)
    related_volunteers = relationship(KitchenVolunteerAssociation)


class Food(db.Model):
    food_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, unique=True)
    measure = db.Column(db.Unicode)
    grams_per_measure = db.Column(db.Float)
    calories = db.Column(db.Float)
    carbs = db.Column(db.Float)
    fat = db.Column(db.Float)
    protein = db.Column(db.Float)
    fiber = db.Column(db.Float)
    calories_per_100g = db.Column(db.Float)
    carbs_per_100g = db.Column(db.Float)
    fat_per_100g = db.Column(db.Float)
    protein_per_100g = db.Column(db.Float)
    fiber_per_100g = db.Column(db.Float)
    img = db.Column(db.Unicode)

    related_kitchens = relationship(KitchenFoodAssociation)
    related_volunteers = relationship(FoodVolunteerAssociation)


class DonationOpportunity(db.Model):
    ein = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)
    address = db.Column(db.Unicode)
    city = db.Column(db.Unicode)
    state = db.Column(db.Unicode)
    zip_code = db.Column(db.Unicode)
    # phone = db.Column(db.Unicode)
    website = db.Column(db.Unicode)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    mission_statement = db.Column(db.Text)
    img = db.Column(db.Unicode)

    deductibility = db.Column(db.Unicode)
    accepting_donations = db.Column(db.Boolean)
    assets = db.Column(db.Float)
    income = db.Column(db.Float)

    related_kitchens = relationship(KitchenVolunteerAssociation)
    related_foods = relationship(FoodVolunteerAssociation)


auth_key = config.get_key("soup")


def check_auth(data=None, **kw):
    if "auth_key" not in data or data["auth_key"] != auth_key:
        raise flask_restless.ProcessingException(description="No auth key", code=401)
    del data["auth_key"]
    return True


def add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Headers"] = "Content-Type"
    return response


app.after_request(add_cors_headers)

# Create the database tables.
db.create_all()

# Create the Flask-Restless API manager.
manager = flask_restless.APIManager(
    app,
    flask_sqlalchemy_db=db,
    preprocessors={"POST": [check_auth], "DELETE": [check_auth]},
)

manager.create_api(
    SoupKitchen,
    methods=["GET", "POST", "DELETE"],
    preprocessors={
        "GET_COLLECTION": [get_many_preprocessor],
        "GET_COLLECTION": [get_many_preprocessor],
    },
    allow_delete_many=True,
    max_results_per_page=1000,
)
manager.create_api(
    Food,
    methods=["GET", "POST", "DELETE"],
    preprocessors={"GET_COLLECTION": [get_many_preprocessor]},
    allow_delete_many=True,
    max_results_per_page=1000,
)
manager.create_api(
    DonationOpportunity,
    methods=["GET", "POST", "DELETE"],
    preprocessors={"GET_COLLECTION": [get_many_preprocessor]},
    allow_delete_many=True,
    max_results_per_page=1000,
)

manager.create_api(
    KitchenFoodAssociation,
    methods=["GET", "POST", "DELETE"],
    preprocessors={"GET_COLLECTION": [get_many_preprocessor]},
    allow_delete_many=True,
    max_results_per_page=1000,
)
manager.create_api(
    KitchenVolunteerAssociation,
    methods=["GET", "POST", "DELETE"],
    preprocessors={"GET_COLLECTION": [get_many_preprocessor]},
    allow_delete_many=True,
    max_results_per_page=1000,
)
manager.create_api(
    FoodVolunteerAssociation,
    methods=["GET", "POST", "DELETE"],
    preprocessors={"GET_COLLECTION": [get_many_preprocessor]},
    allow_delete_many=True,
    max_results_per_page=1000,
)

if __name__ == "__main__":
    # app.run(host="127.0.0.1", port = 8000)
    app.run(host="127.0.0.1", port=8000, debug=True)
