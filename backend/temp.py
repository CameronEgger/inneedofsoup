import requests
import json

url = "http://127.0.0.1:8000/api/soup_kitchen"
headers = {"Content-Type": "application/vnd.api+json"}

filters = [dict(name="name", op="ilike", val="%%")]
sort = [dict(name="zip_code")]
params = dict(sort=sort)

response = requests.get(url, params=params, headers=headers)
print(response.status_code)
assert response.status_code == 200
print(response.json())
