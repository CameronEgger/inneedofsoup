import sys, os

# If keys are local, you can pass as an argument the location of the keys

key_location = "keys"
if len(sys.argv) > 1:
    key_location = sys.argv[1]


def get_key(key_name):
    with open(os.path.join(key_location, key_name)) as f:
        return f.read().strip()
