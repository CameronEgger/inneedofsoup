import main as backend_main
from unittest import main, TestCase
import config
import requests  # noqa
import collections
import flask_restless
import sys
import json
import re

sys.path.append("../")
url = "https://www.inneedofsoup.xyz/"


class BackendUnitTests(TestCase):
    #    def
    def test_kitchen_model(self):  # test1
        kitchen = backend_main.SoupKitchen
        assert hasattr(kitchen, "__dict__")
        db_info = kitchen.__dict__
        assert "__tablename__" in db_info
        assert "__table__" in db_info
        db_table = db_info["__table__"]
        db_table = eval(str(db_table._columns))
        assert db_table == [
            "soup_kitchen.ein",
            "soup_kitchen.name",
            "soup_kitchen.address",
            "soup_kitchen.city",
            "soup_kitchen.state",
            "soup_kitchen.zip_code",
            "soup_kitchen.website",
            "soup_kitchen.latitude",
            "soup_kitchen.longitude",
            "soup_kitchen.mission_statement",
            "soup_kitchen.img",
        ]

    def test_food_model(self):  # test2
        food = backend_main.Food
        assert hasattr(food, "__dict__")
        db_info = food.__dict__
        assert "__tablename__" in db_info
        assert "__table__" in db_info
        db_table = db_info["__table__"]
        db_table = eval(str(db_table._columns))
        assert db_table == [
            "food.food_id",
            "food.name",
            "food.measure",
            "food.grams_per_measure",
            "food.calories",
            "food.carbs",
            "food.fat",
            "food.protein",
            "food.fiber",
            "food.calories_per_100g",
            "food.carbs_per_100g",
            "food.fat_per_100g",
            "food.protein_per_100g",
            "food.fiber_per_100g",
            "food.img",
        ]

    def test_donation_model(self):  # test3
        do = backend_main.DonationOpportunity
        assert hasattr(do, "__dict__")
        db_info = do.__dict__
        assert "__tablename__" in db_info
        assert "__table__" in db_info
        db_table = db_info["__table__"]
        db_table = eval(str(db_table._columns))
        assert db_table == [
            "donation_opportunity.ein",
            "donation_opportunity.name",
            "donation_opportunity.address",
            "donation_opportunity.city",
            "donation_opportunity.state",
            "donation_opportunity.zip_code",
            "donation_opportunity.website",
            "donation_opportunity.latitude",
            "donation_opportunity.longitude",
            "donation_opportunity.mission_statement",
            "donation_opportunity.img",
            "donation_opportunity.deductibility",
            "donation_opportunity.accepting_donations",
            "donation_opportunity.assets",
            "donation_opportunity.income",
        ]

    def test_auth_key(self):  # test4
        func = backend_main.check_auth
        try:
            data = {
                "key": 1
            }  # 1 can never be a key as it fails to meet length requirement
            func(data)
            assert False
        except flask_restless.ProcessingException:
            pass

    def test_add_cors_headers(self):  # test5
        func = backend_main.add_cors_headers

        class temp:
            def __init__(self):
                self.headers = {}

        response = temp()
        assert "Access-Control-Allow-Origin" not in response.headers
        assert "Access-Control-Allow-Credentials" not in response.headers
        response = func(response)
        assert (
            "Access-Control-Allow-Origin" in response.headers
            and response.headers["Access-Control-Allow-Origin"] == "*"
        )
        assert (
            "Access-Control-Allow-Credentials" in response.headers
            and response.headers["Access-Control-Allow-Credentials"] == "true"
        )

    def test_site_up(self):  # test6
        response = requests.request("GET", "https://api.inneedofsoup.xyz/api/donation_opportunity")
        self.assertTrue(response.ok)

    def test_api_up(self):  # test7
        # this is intended to not return anything
        response = requests.request("GET", "https://api.inneedofsoup.xyz/api/")
        self.assertFalse(response.ok)

    def test_api_food_up(self):  # test8
        response = requests.request("GET", "https://api.inneedofsoup.xyz/api/food")
        self.assertTrue(response.ok)

    def test_api_kitchen_up(self):  # test9
        response = requests.request(
            "GET", "https://api.inneedofsoup.xyz/api/soup_kitchen"
        )
        self.assertTrue(response.ok)

    def test_api_volunteer_up(self):  # test10
        response = requests.request(
            "GET", "https://api.inneedofsoup.xyz/api/donation_opportunity"
        )
        self.assertTrue(response.ok)

    def test_api_volunteer_instance_up(self):  # test11
        response = requests.request(
            "GET", "https://api.inneedofsoup.xyz/api/donation_opportunity/30223860"
        )
        self.assertTrue(response.ok)

    def test_api_food_instance_up(self):  # test12
        response = requests.request("GET", "https://api.inneedofsoup.xyz/api/food/9001")
        self.assertTrue(response.ok)

    def test_api_kitchen_instance_up(self):  # test13
        response = requests.request(
            "GET", "https://api.inneedofsoup.xyz/api/soup_kitchen/61071228"
        )
        self.assertTrue(response.ok)

    def test_api_kitchen_search_integrity(self):  # test14
        url = "https://api.inneedofsoup.xyz/api/soup_kitchen"
        headers = {"Content-Type": "application/vnd.api+json"}
        filters = [dict(name="name", op="ilike", val=r"%")]
        params = dict(q=json.dumps(dict(filters=filters)))
        response = requests.get(url, params=params, headers=headers)
        self.assertTrue(response.ok)

    def test_api_kitchen_search(self):  # test15
        url = "https://api.inneedofsoup.xyz/api/soup_kitchen"
        headers = {"Content-Type": "application/vnd.api+json"}
        filters = [dict(name="name", op="ilike", val=r"%paw%")]
        params = dict(q=json.dumps(dict(filters=filters)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()
        self.assertTrue(re.search(".PAW.", str(result)) is not None)

    def test_api_nutrition_search(self):  # test16
        url = "https://api.inneedofsoup.xyz/api/food"
        headers = {"Content-Type": "application/vnd.api+json"}
        filters = [dict(name="calories", op="==", val=56.0)]
        params = dict(q=json.dumps(dict(filters=filters)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()
        self.assertTrue(re.search(".56\.0.", str(result)) is not None)

    def test_api_volunteer_search(self):  # test17
        url = "https://api.inneedofsoup.xyz/api/donation_opportunity"
        headers = {"Content-Type": "application/vnd.api+json"}
        filters = [dict(name="ein", op="==", val=10352636)]
        params = dict(q=json.dumps(dict(filters=filters)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()
        self.assertTrue(re.search(".10352636.", str(result)) is not None)

    def test_api_kitchen_search_ignore_case(self):  # test18
        url = "https://api.inneedofsoup.xyz/api/soup_kitchen"
        headers = {"Content-Type": "application/vnd.api+json"}
        filters = [dict(name="name", op="ilike", val=r"%paw%")]
        params = dict(q=json.dumps(dict(filters=filters)))
        response_lower = requests.get(url, params=params, headers=headers)
        result_lower = response_lower.json()
        filters = [dict(name="name", op="ilike", val=r"%PAW%")]
        params = dict(q=json.dumps(dict(filters=filters)))
        response_higher = requests.get(url, params=params, headers=headers)
        result_higher = response_higher.json()
        self.assertTrue(result_lower == result_higher)

    def test_api_kitchen_filter_mission_statement(self):  # test19
        url = "https://api.inneedofsoup.xyz/api/soup_kitchen"
        headers = {"Content-Type": "application/vnd.api+json"}
        filters = [dict(name="mission_statement", op="neq", val=r"")]
        params = dict(q=json.dumps(dict(filters=filters)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()["objects"]
        for item in result:
            self.assertTrue(item["mission_statement"] != "")

    def test_api_kitchen_sorting(self):  # test20
        url = "https://api.inneedofsoup.xyz/api/soup_kitchen"
        headers = {"Content-Type": "application/vnd.api+json"}
        order_by = [dict(field="name", direction="asc")]
        params = dict(q=json.dumps(dict(order_by=order_by)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()
        result = result["objects"]
        cur_name = result[0]["name"]
        for item in result:
            self.assertTrue(cur_name <= item["name"])
            cur_name = item["name"]

    def test_api_food_sorting_desc(self):  # test21
        url = "https://api.inneedofsoup.xyz/api/food"
        headers = {"Content-Type": "application/vnd.api+json"}
        order_by = [dict(field="calories", direction="desc")]
        params = dict(q=json.dumps(dict(order_by=order_by)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()
        result = result["objects"]
        cur_calories = result[0]["calories"]
        for item in result:
            self.assertTrue(cur_calories >= item["calories"])
            cur_calories = item["calories"]

    def test_api_volunteer_sorting(self):  # test22
        url = "https://api.inneedofsoup.xyz/api/soup_kitchen"
        headers = {"Content-Type": "application/vnd.api+json"}
        order_by = [dict(field="ein", direction="asc")]
        params = dict(q=json.dumps(dict(order_by=order_by)))
        response = requests.get(url, params=params, headers=headers)
        result = response.json()
        result = result["objects"]
        cur_ein = result[0]["ein"]
        for item in result:
            self.assertTrue(cur_ein <= item["ein"])
            cur_ein = item["ein"]


if __name__ == "__main__":
    main()
